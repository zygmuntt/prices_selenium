﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.Tesco
{
  public  class Run_pieczywo_cukiernia
    {
        [Theory]
        [InlineData(Tesco.URLS.pieczywo_cukiernia)]
        public void Run(string url)
        {
            var go = new Check_prices_smart();
            go.Check_prices(url);
        }
    }
}
