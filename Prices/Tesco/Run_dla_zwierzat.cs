﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.Tesco
{
    public class Run_dla_zwierzat
    {
        [Theory]
        [InlineData(Tesco.URLS.dla_zwierzat)]
        public void Run(string url)
        {
            var go = new Check_prices_smart();
            go.Check_prices(url);
        }
    }
}
