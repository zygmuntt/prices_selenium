﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.Tesco
{
    public class Run_nabial_i_jaja
    {
        [Theory]
        [InlineData(Tesco.URLS.nabial_i_jaja)]
        public void Run(string url)
        {
            var go = new Check_prices_smart();
            go.Check_prices(url);
        }
    }
}
