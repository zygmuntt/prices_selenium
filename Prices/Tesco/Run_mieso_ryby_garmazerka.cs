﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.Tesco
{
    public class Run_mieso_ryby_garmazerka
    {
        [Theory]
        [InlineData(Tesco.URLS.mieso_ryby_garmazerka)]
        public void Run(string url)
        {
            var go = new Check_prices_smart();
            go.Check_prices(url);
        }
    }
}
