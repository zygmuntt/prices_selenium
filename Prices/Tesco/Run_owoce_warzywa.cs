﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.Tesco
{
    public class Run_owoce_warzywa
    {
        [Theory]
        [InlineData(Tesco.URLS.owoce_warzywa)]
        public void Run(string url)
        {
            var go = new Check_prices_smart();
            go.Check_prices(url);
        }
    }
}
