﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Prices.Tesco
{
    public class Check_prices_smart
    {
        private IWebDriver driver;
        public void Check_prices(string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            int page = 1;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            string page_url = "";
            for (int i=2;i>1;i++)
            {
                if (page>=100)
                {
                    page++;
                    string page_str = page.ToString();
                    page_url = driver.Url;
                    page_url = page_url.Substring(0, page_url.Length - 9);
                    page_url = page_url + $"?page={page_str}";
                }
                if (page >= 10 && page<100)
                {
                    page++;
                    string page_str = page.ToString();
                    page_url = driver.Url;
                    page_url=  page_url.Substring(0, page_url.Length - 8);
                    page_url = page_url + $"?page={page_str}";
                }
                if (page < 10 && page > 1)
                {
                    page++;
                    string page_str = page.ToString();
                    page_url = driver.Url;
                    page_url =  page_url.Substring(0, page_url.Length - 7);
                    page_url = page_url + $"?page={page_str}";
                }
                if (page == 1)
                {
                    page++;
                    string page_str = page.ToString();
                    page_url = driver.Url;
                    page_url = page_url + $"?page={page_str}";
                }


                    IList<IWebElement> articles = driver.FindElements(By.XPath("//a[@class='product-tile--title product-tile--browsable']"));
                    IList<IWebElement> prices = driver.FindElements(By.XPath("//div[contains(@class,'price-per-sellable-unit price-per-sellable-unit--price price-per-sellable-unit')]//span[@class='value' and @data-auto='price-value']"));
                    IList<IWebElement> local_ids = driver.FindElements(By.XPath("//div[@class='tile-content']//a[@class='product-image-wrapper']"));
                    IList<IWebElement> prices_per_unit = driver.FindElements(By.XPath("//div[@class='price-details--wrapper']//div[@class='price-per-quantity-weight']//span[@data-auto='price-value']"));
                    for(int j=0;j<articles.Count;j++)
                    {
                    string name = articles[j].Text;
                    string price = prices[j].Text;
                    string id = local_ids[j].GetAttribute("href");
                    id = id.Replace("https://ezakupy.tesco.pl/groceries/pl-PL/products/", "");
                    string price_per_unit = prices_per_unit[j].Text;
                    price_per_unit = price_per_unit.Replace(",", ".");
                    price = price.Replace(",", ".");
                    name = name.Replace("'", ".");
                    AGAIN:
                    try
                    {
                        SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, Tesco_price, Tesco_price_per_unit, Tesco_local_id) values('{name}', '{price}','{price_per_unit}', '{id}') else update prices.dbo.Articles set Tesco_price_per_unit = '{price_per_unit}', Tesco_price = '{price}', Tesco_local_id = '{id}' where name = '{name}'");
                    }
                    catch (System.Data.SqlClient.SqlException deadlock)
                    {
                        if (deadlock.Number == 1205)
                            goto AGAIN;
                    }
                    AGAIN2:
                    try
                    {
                        SQL.Insert($"begin if not exists (select* from [prices].[dbo].[Tesco] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}' and price_per_unit= '{price_per_unit}') begin Insert into [prices].[dbo].[Tesco] (local_id,price,date,name,price_per_unit) values ('{id}','{price}','{time}','{name}','{price_per_unit}') end end ");
                    }
                    catch (System.Data.SqlClient.SqlException deadlock)
                    {
                        if (deadlock.Number == 1205)
                            goto AGAIN2;
                    }
                }
                    if(articles.Count ==0)
                    {
                    break;
                    }
                driver.Url = page_url;


            }
            driver.Quit();



        }




    }
}
