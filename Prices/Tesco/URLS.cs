﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prices.Tesco
{
   public class URLS
    {
        public const string art_przemyslowe = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/art.-przemyslowe/all";
        public const string owoce_warzywa = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/owoce-warzywa/all";
        public const string nabial_i_jaja = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/nabial-i-jaja/all";
        public const string pieczywo_cukiernia = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/pieczywo-cukiernia/all";
        public const string mieso_ryby_garmazerka = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/mieso-ryby-garmaz/all";
        public const string art_spozywcze = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/art.-spozywcze/all";
        public const string mrozonki = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/mrozonki/all";
        public const string napoje = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/napoje/all";
        public const string chemia = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/chemia/all";
        public const string kosmetyki = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/kosmetyki/all";
        public const string dla_dzieci = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/dla-dzieci/all";
        public const string dla_zwierzat = "https://ezakupy.tesco.pl/groceries/pl-PL/shop/dla-zwierzat/all";
        

    }
}
