﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
namespace Prices.EuroRTVAGD
{
  public  class Euro_rtv_agd_check_prices
    {

        private IWebDriver driver;


        public void Check_prices_smart (string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            IList<IWebElement> main_window_categories = driver.FindElements(By.XPath("//a[@class='category-list-box-link selenium-LP-category-list-box-link']"));
            for (int i = 0; i < main_window_categories.Count; i++)//glowne kategorie
            {
                System.Threading.Thread.Sleep(1000);
                main_window_categories = driver.FindElements(By.XPath("//a[@class='category-list-box-link selenium-LP-category-list-box-link']"));
                main_window_categories[i].Click();
                System.Threading.Thread.Sleep(4000);
                IList<IWebElement> second_window_categories = driver.FindElements(By.XPath("//span[@class='category-list-box-name selenium-LP-product-sub-category-name']"));
                string url_podkategorii = driver.Url;
                if (second_window_categories.Count == 0) //brak podkategorii
                {
                    //zbieranie itemow 
                    int next_pages = 1;
                    for (int l = 1; l > 0; l++)
                    {
                        IList<IWebElement> names = driver.FindElements(By.XPath("//a[@class='js-save-keyword selenium-LP-product-link']"));
                        IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='product-sales-category is-active']//div//div[@class='price-normal selenium-price-normal']"));
                        IList<IWebElement> local_ids = driver.FindElements(By.XPath("//div[@class='selenium-product-code']"));
                        string category = null;
                        try
                        {
                             category = driver.FindElement(By.CssSelector("#products-header > h1")).Text;
                        }
                        catch { }
                        for (int k = 0; k < prices.Count; k++)
                        {
                            var name = names[k].Text;
                            name = name.Replace("'", "");
                            var price = prices[k].Text;
                            if (name.Length == 0)
                            {
                                goto NEXT_PAGE;
                            }
                            price = price.Remove(price.Length - 3);
                            price = price.Replace(" ", "");
                            var id = local_ids[k].Text;
                            AGAIN:
                            try
                            {
                                SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}' and EuroRTVAGD_category like '{category}') insert into prices.dbo.Articles(name, Price_EuroRTVAGD, EuroRTVAGD_Local_id, EuroRTVAGD_category) values('{name}', '{price}', '{id}','{category}') else update prices.dbo.Articles set eurortvagd_category = '{category}', Price_EuroRTVAGD = '{price}', EuroRTVAGD_local_id = '{id}' where name = '{name}'");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN;
                            }
                            AGAIN2:
                            try
                            {
                                SQL.Insert($"begin if not exists (select* from [prices].[dbo].[EuroRTVAGD] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[EuroRTVAGD] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN2;
                            }
                        }
                        //nastepna strona
                        string url_strony = driver.Url;
                        string next_url = url_strony;
                        if (next_pages == 1)
                        {
                            next_url = next_url.Remove(next_url.Length - 6);
                        }
                        next_pages++;
                        string next_pages_str = next_pages.ToString();
                        if (next_pages > 1)
                        {
                            try
                            {
                                int next_pages2 = next_pages - 1;
                                string next_pages_str2 = next_pages2.ToString();
                                next_url = next_url.Replace($",strona-{next_pages_str2}.bhtml", "");
                            }
                            catch { }
                        }
                        string temp_url = $",strona-{next_pages_str}.bhtml";
                        driver.Url = next_url + temp_url;
                        IList<IWebElement> check_break = driver.FindElements(By.XPath("//a[@class='js-save-keyword selenium-LP-product-link']"));
                        if (check_break.Count < 1)
                            break;

                    }
                }
                else //istnieja podkategorie
                {

                    for (int j = 0; j < second_window_categories.Count; j++)
                    {
                        System.Threading.Thread.Sleep(2000);
                        second_window_categories = driver.FindElements(By.XPath("//span[@class='category-list-box-name selenium-LP-product-sub-category-name']"));
                        second_window_categories[j].Click(); //wejscie w podkategorie
                        var gry_planszowe = "https://www.euro.com.pl/gry1.bhtml";
                        if(driver.Url == gry_planszowe)
                        {
                            driver.FindElement(By.CssSelector("#category-list > div.category-list-container > div > div > h2 > a > img")).Click();
                        }
                        int next_pages = 1;
                        System.Threading.Thread.Sleep(2000);   
                        //zbieranie itemow
                        for (int l = 1; l > 0; l++)
                        { 
                            IList<IWebElement> names = driver.FindElements(By.XPath("//a[@class='js-save-keyword selenium-LP-product-link']"));
                            IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='product-sales-category is-active']//div//div[@class='price-normal selenium-price-normal']"));
                            IList<IWebElement> local_ids = driver.FindElements(By.XPath("//div[@class='selenium-product-code']"));
                            string category = null;
                            try
                            {
                               category = driver.FindElement(By.CssSelector("#products-header > h1")).Text;
                            }
                            catch { }
                            for (int k = 0; k < prices.Count; k++)
                            {
                                var name = names[k].Text;
                                name = name.Replace("'", "");
                                if (name.Length == 0)
                                {
                                    goto NEXT_SUBCAT;
                                }
                                var price = prices[k].Text;
                                price = price.Remove(price.Length - 3);
                                price = price.Replace(" ", "");
                                var id = local_ids[k].Text;
                                AGAIN:
                                try
                                {
                                 SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}' and EuroRTVAGD_category like '{category}') insert into prices.dbo.Articles(name, Price_EuroRTVAGD, EuroRTVAGD_Local_id, EuroRTVAGD_category) values('{name}', '{price}', '{id}','{category}') else update prices.dbo.Articles set eurortvagd_category = '{category}', Price_EuroRTVAGD = '{price}', EuroRTVAGD_local_id = '{id}' where name = '{name}'");
                                }
                                catch (System.Data.SqlClient.SqlException deadlock)
                                {
                                    if (deadlock.Number == 1205)
                                        goto AGAIN;
                                }
                                AGAIN2:
                                try
                                {
                                    SQL.Insert($"begin if not exists (select* from [prices].[dbo].[EuroRTVAGD] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[EuroRTVAGD] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                                }
                                catch (System.Data.SqlClient.SqlException deadlock)
                                {
                                    if (deadlock.Number == 1205)
                                        goto AGAIN2;
                                }
                            }
                            //nastepna strona
                            string url_strony = driver.Url;
                            string next_url = url_strony;
                            if (next_pages == 1)
                            {
                                next_url = next_url.Remove(next_url.Length - 6);
                            }
                            next_pages++;
                            string next_pages_str = next_pages.ToString();
                            if (next_pages > 1)
                            {
                                try
                                {
                                    int next_pages2 = next_pages - 1;
                                    string next_pages_str2 = next_pages2.ToString();
                                    next_url = next_url.Replace($",strona-{next_pages_str2}.bhtml", "");
                                }
                                catch { }
                            }
                            string temp_url = $",strona-{next_pages_str}.bhtml";
                            driver.Url = next_url + temp_url;
                            IList<IWebElement> check_break = driver.FindElements(By.XPath("//a[@class='js-save-keyword selenium-LP-product-link']"));
                            if (check_break.Count < 1 || url_strony== "https://www.euro.com.pl/etui-do-telefonow.bhtml" || url_strony == "https://www.euro.com.pl/worki-i-filtry-do-odkurzaczy.bhtml" || url_strony== "https://www.euro.com.pl/uchwyty-do-telewizorow.bhtml" || url_strony == "https://www.euro.com.pl/tusze-i-tonery1.bhtml")
                                break;
                        }
                        NEXT_SUBCAT:
                        driver.Url = url_podkategorii;
                    }
                    driver.Url = url;
                }
                NEXT_PAGE:
                driver.Url = url;
            }
            driver.Quit();
        }
    }
}
