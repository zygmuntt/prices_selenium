﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.EuroRTVAGD
{
    public class Run_etui_like
    {
        [Theory]
        [InlineData(URLS.cyfrowe_dodatki_do_gier_xbox)]
        [InlineData(URLS.Etui_ipod)]
        [InlineData(URLS.gry_nintendo_switch)]
        [InlineData(URLS.gry_pc)]
        [InlineData(URLS.gry_playstation4)]
        [InlineData(URLS.gry_playstation_3)]
        [InlineData(URLS.gry_playstation_vita)]
        [InlineData(URLS.gry_psp)]
        [InlineData(URLS.gry_xbox_360)]
        [InlineData(URLS.gry_xbox_one)]
        [InlineData(URLS.kable_antentowe)]
        [InlineData(URLS.kable_audio_wideo)]
        [InlineData(URLS.kable_do_telefonow)]
        [InlineData(URLS.kable_glosikowe)]
        [InlineData(URLS.kable_ipod_iphone_ipad)]
        [InlineData(URLS.kable_komputerowe)]
        [InlineData(URLS.kable_zasilajace)]
        [InlineData(URLS.ladowarki)]
        [InlineData(URLS.ladowarki_ipod_iphone)]
        [InlineData(URLS.ladowarki_sieciowe)]
        [InlineData(URLS.ladwowarki_samochodowe)]
        [InlineData(URLS.playstation_network)]
        [InlineData(URLS.torby_do_aparatow_i_kamer)]
        [InlineData(URLS.torby_do_laptopow)]
        [InlineData(URLS.Xbox_live)]
        public void Check_etui_like(string url)
        {
            var go = new Euro_rtv_agd_check_prices2();
            go.check_prices_etui_like(url);
        }

        [Theory]
        [InlineData("https://www.euro.com.pl/etui-do-telefonow,strona-1.bhtml")]
        [InlineData("https://www.euro.com.pl/etui-do-ipada-i-tabletow-multimedialnych,strona-1.bhtml")]
        public void Check_etui(string url)
        {
            var go = new Euro_rtv_agd_check_prices2();
            go.check_prices_etui(url);
        }

        [Theory]
        [InlineData(URLS.worki_i_filtry_do_odkurzaczy)]
        [InlineData(URLS.uchwyty_do_telewizorow)]
        [InlineData(URLS.folie_i_ostrza_do_golarek)]
        [InlineData(URLS.tusze_i_tonery)]
        public void check_uchwyt(string url)
        {
            var go = new Euro_rtv_agd_check_prices2();
            go.check_prices_uchwyty(url);
        }

    }
}
