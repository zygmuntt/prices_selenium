﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace Prices.EuroRTVAGD
{
    public class Euro_rtv_agd_check_prices2
    {

        private IWebDriver driver;
        public void check_prices_etui_like(string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            int next_pages = 1;
            {
                //jebane etui
                for (int l = 1; l > 0; l++)
                {
                    System.Threading.Thread.Sleep(1000);
                    IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='product-main']//div[starts-with(@class,'product-price  price-show')]//div[@class='price-normal selenium-price-normal']"));
                    IList<IWebElement> names = driver.FindElements(By.XPath("//div[@class='product-main']//h2[@class='product-name']//a[@class='js-save-keyword']"));
                    IList<IWebElement> local_ids = driver.FindElements(By.XPath("//div[starts-with(@class,'product-tile js-UA-product')]"));
                    var category = driver.FindElement(By.CssSelector("#products-header > h1")).Text;  
                    for (int k = 0; k < prices.Count; k++)
                    {
                        var name = names[k].Text;
                        name = name.Replace("'", "");
                        var price = prices[k].Text;
                        price = price.Remove(price.Length - 3);
                        price = price.Replace(" ", "");
                        price = price.Replace(",", ".");
                        string id = local_ids[k].GetAttribute("data-product-id");
                        AGAIN:
                        try
                        {
                                                           SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}' and EuroRTVAGD_category like '{category}') insert into prices.dbo.Articles(name, Price_EuroRTVAGD, EuroRTVAGD_Local_id, EuroRTVAGD_category) values('{name}', '{price}', '{id}','{category}') else update prices.dbo.Articles set eurortvagd_category = '{category}', Price_EuroRTVAGD = '{price}', EuroRTVAGD_local_id = '{id}' where name = '{name}'");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN;
                        }
                        AGAIN2:
                        try
                        {
                            SQL.Insert($"begin if not exists (select* from [prices].[dbo].[EuroRTVAGD] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[EuroRTVAGD] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN2;
                        }
                    }
                    string url_strony = driver.Url;
                    string next_url = url_strony;
                    if (next_pages == 1)
                    {
                        next_url = next_url.Remove(next_url.Length - 6);
                    }
                    next_pages++;
                    string next_pages_str = next_pages.ToString();
                    if (next_pages > 1)
                    {
                        try
                        {
                            int next_pages2 = next_pages - 1;
                            string next_pages_str2 = next_pages2.ToString();
                            next_url = next_url.Replace($",strona-{next_pages_str2}.bhtml", "");
                        }
                        catch { }
                    }
                    string temp_url = $",strona-{next_pages_str}.bhtml";
                    driver.Url = next_url + temp_url;
                    IList<IWebElement> check_break = driver.FindElements(By.XPath("//div[@class='product-main']//h2[@class='product-name']//a[@class='js-save-keyword']"));
                    if (check_break.Count < 1)
                        break;
                }
                driver.Quit();
            }
        }
        public void check_prices_etui(string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            int next_pages = 1;
            {
                //jebane etui
                for (int l = 1; l > 0; l++)
                {
                    System.Threading.Thread.Sleep(1000);
                    IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='product-main']//div[@class='product-price  price-show']//div[@class='price-normal selenium-price-normal']"));
                    IList<IWebElement> names = driver.FindElements(By.XPath("//div[@class='product-main']//h2[@class='product-name']//a[@class='js-save-keyword']"));
                    IList<IWebElement> local_ids = driver.FindElements(By.XPath("//div[starts-with(@class,'product-tile js-UA-product')]"));
                    var category = driver.FindElement(By.CssSelector("#products-header > h1")).Text;
                    for (int k = 0; k < prices.Count; k++)
                    {
                        var name = names[k].Text;
                        name = name.Replace("'", "");
                        var price = prices[k].Text;
                        price = price.Remove(price.Length - 3);
                        price = price.Replace(" ", "");
                        price = price.Replace(",", ".");
                        string id = local_ids[k].GetAttribute("data-product-id");
                        AGAIN:
                        try
                        {
                                                           SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}' and EuroRTVAGD_category like '{category}') insert into prices.dbo.Articles(name, Price_EuroRTVAGD, EuroRTVAGD_Local_id, EuroRTVAGD_category) values('{name}', '{price}', '{id}','{category}') else update prices.dbo.Articles set eurortvagd_category = '{category}', Price_EuroRTVAGD = '{price}', EuroRTVAGD_local_id = '{id}' where name = '{name}'");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN;
                        }
                        AGAIN2:
                        try
                        {
                            SQL.Insert($"begin if not exists (select* from [prices].[dbo].[EuroRTVAGD] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[EuroRTVAGD] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN2;
                        }
                    }
                    string url_strony = driver.Url;
                    string next_url = url_strony;
                    if (next_pages == 1)
                    {
                        next_url = next_url.Remove(next_url.Length - 15);
                    }
                    next_pages++;
                    string next_pages_str = next_pages.ToString();
                    if (next_pages > 1)
                    {
                        try
                        {
                            int next_pages2 = next_pages - 1;
                            string next_pages_str2 = next_pages2.ToString();
                            next_url = next_url.Replace($",strona-{next_pages_str2}.bhtml", "");
                        }
                        catch { }
                    }
                    string temp_url = $",strona-{next_pages_str}.bhtml";
                    driver.Url = next_url + temp_url;
                    IList<IWebElement> check_break = driver.FindElements(By.XPath("//div[@class='product-main']//h2[@class='product-name']//a[@class='js-save-keyword']"));
                    if (check_break.Count < 1)
                        break;
                }
                driver.Quit();
            }

        }





        public void check_prices_uchwyty(string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            int next_pages = 1;
            {
                //jebane etui
                for (int l = 1; l > 0; l++)
                {
                    System.Threading.Thread.Sleep(1000);
                        IList<IWebElement> names = driver.FindElements(By.XPath("//a[@class='js-save-keyword selenium-LP-product-link']"));
                        IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='product-sales-category is-active']//div//div[@class='price-normal selenium-price-normal']"));
                        IList<IWebElement> local_ids = driver.FindElements(By.XPath("//div[@class='js-show-tooltip best-voucher']"));
                        var category = driver.FindElement(By.CssSelector("#products-header > h1")).Text;
                    for (int k = 0; k < prices.Count; k++)
                    {
                        var name = names[k].Text;
                        name = name.Replace("'", "");
                        var price = prices[k].Text;
                        price = price.Remove(price.Length - 3);
                        price = price.Replace(" ", "");
                        price = price.Replace(",", ".");
                        string id = local_ids[k].GetAttribute("data-id");
                        AGAIN:
                        try
                        {
                                                           SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}' and EuroRTVAGD_category like '{category}') insert into prices.dbo.Articles(name, Price_EuroRTVAGD, EuroRTVAGD_Local_id, EuroRTVAGD_category) values('{name}', '{price}', '{id}','{category}') else update prices.dbo.Articles set eurortvagd_category = '{category}', Price_EuroRTVAGD = '{price}', EuroRTVAGD_local_id = '{id}' where name = '{name}'");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN;
                        }
                        AGAIN2:
                        try
                        {
                            SQL.Insert($"begin if not exists (select* from [prices].[dbo].[EuroRTVAGD] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[EuroRTVAGD] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN2;
                        }
                    }
                    string url_strony = driver.Url;
                    string next_url = url_strony;
                    if (next_pages == 1)
                    {
                        next_url = next_url.Remove(next_url.Length - 15);
                    }
                    next_pages++;
                    string next_pages_str = next_pages.ToString();
                    if (next_pages > 1)
                    {
                        try
                        {
                            int next_pages2 = next_pages - 1;
                            string next_pages_str2 = next_pages2.ToString();
                            next_url = next_url.Replace($",strona-{next_pages_str2}.bhtml", "");
                        }
                        catch { }
                    }
                    string temp_url = $",strona-{next_pages_str}.bhtml";
                    driver.Url = next_url + temp_url;
                    IList<IWebElement> check_break = driver.FindElements(By.XPath("//div[@class='product-main']//h2[@class='product-name']//a[@class='js-save-keyword']"));
                    if (check_break.Count < 1)
                        break;
                }
                driver.Quit();
            }

        }








    }
}
