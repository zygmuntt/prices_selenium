﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.EuroRTVAGD
{
   public class Run_tv_audio_i_video
    {
        [Fact]
        public void Run()
        {
            string url = "https://www.euro.com.pl/rtv.bhtml";
            var go = new Euro_rtv_agd_check_prices();
            go.Check_prices_smart(url);
        }
    }
}
