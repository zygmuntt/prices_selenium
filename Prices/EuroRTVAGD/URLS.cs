﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prices.EuroRTVAGD
{
   public class URLS
    {
        public const string agd = "https://www.euro.com.pl/agd.bhtml";
        public const string rtv = "https://www.euro.com.pl/rtv.bhtml";
        public const string agd_do_zabudowy = "https://www.euro.com.pl/agd-do-zabudowy.bhtml";
        public const string agd_male = "https://www.euro.com.pl/agd-male.bhtml";
        public const string komputery = "https://www.euro.com.pl/komputery.bhtml";
        public const string gry_i_konsole = "https://www.euro.com.pl/gry-i-konsole.bhtml";
        public const string foto_i_kamery = "https://www.euro.com.pl/foto-i-kamery.bhtml";
        public const string telefony_i_zegarki = "https://www.euro.com.pl/akcesoria-do-telefonow.bhtml?link=mainnavi";

        //ala etui

        public const string kable_audio_wideo = "https://www.euro.com.pl/kable.bhtml";
        public const string ladowarki_ipod_iphone = "https://www.euro.com.pl/ladowarki-ipod-iphone1.bhtml";
        public const string ladowarki = "https://www.euro.com.pl/ladowarki4.bhtml";
        public const string kable_ipod_iphone_ipad = "https://www.euro.com.pl/kable-apple-ipod-iphone-ipad.bhtml";
        public const string kable_antentowe = "https://www.euro.com.pl/kable-antenowe.bhtml";
        public const string kable_zasilajace = "https://www.euro.com.pl/kable-zasilajace.bhtml";
        public const string kable_glosikowe = "https://www.euro.com.pl/kable-glosnikowe.bhtml";
        public const string Etui_ipod = "https://www.euro.com.pl/etui-ipod-iphone1.bhtml";
        public const string kable_komputerowe = "https://www.euro.com.pl/kable-komputerowe.bhtml";
        public const string kable_do_telefonow = "https://www.euro.com.pl/kable-do-telefonow1.bhtml";
        public const string ladwowarki_samochodowe = "https://www.euro.com.pl/ladowarki-samochodowe1.bhtml";
        public const string Xbox_live = "https://www.euro.com.pl/xbox-live1.bhtml";
        public const string ladowarki_sieciowe = "https://www.euro.com.pl/ladowarki-sieciowe.bhtml";
        public const string gry_xbox_one = "https://www.euro.com.pl/gry-xbox-one.bhtml";
        public const string cyfrowe_dodatki_do_gier_xbox = "https://www.euro.com.pl/cyfrowe-dodatki-do-gier-xbox.bhtml";
        public const string gry_playstation4 = "https://www.euro.com.pl/gry-playstation-4.bhtml";
        public const string gry_nintendo_switch = "https://www.euro.com.pl/gry-nintendo-switch.bhtml";
        public const string torby_do_aparatow_i_kamer = "https://www.euro.com.pl/torby.bhtml";
        public const string gry_xbox_360 = "https://www.euro.com.pl/gry-xbox-360.bhtml";
        public const string gry_pc = "https://www.euro.com.pl/gry-pc.bhtml";
        public const string playstation_network = "https://www.euro.com.pl/playstation-network2.bhtml";
        public const string gry_playstation_vita = "https://www.euro.com.pl/gry-playstation-vita.bhtml";
        public const string gry_playstation_3 = "https://www.euro.com.pl/gry-playstation-3.bhtml";
        public const string gry_psp = "https://www.euro.com.pl/gry-playstation-portable-psp.bhtml";
        public const string torby_do_laptopow = "https://www.euro.com.pl/torby-do-laptopow1.bhtml";

        //ala uchwyty
        public const string folie_i_ostrza_do_golarek = "https://www.euro.com.pl/folie-i-ostrza-do-golarek,strona-1.bhtml";
        public const string worki_i_filtry_do_odkurzaczy = "https://www.euro.com.pl/worki-i-filtry-do-odkurzaczy,strona-1.bhtml";
        public const string uchwyty_do_telewizorow = "https://www.euro.com.pl/uchwyty-do-telewizorow,a1:2,strona-1.bhtml";
        public const string tusze_i_tonery = "https://www.euro.com.pl/tusze-i-tonery,strona-1.bhtml";
    }
}
