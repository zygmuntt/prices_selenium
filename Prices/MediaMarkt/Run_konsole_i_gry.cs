﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_konsole_i_gry
    {
        [Theory]
        [InlineData(URLS.konsole_i_gry_akcesoria_do_konsol, URLS.baza_konsole_i_gry)]
        [InlineData(URLS.konsole_i_gry_bestsellery, URLS.baza_konsole_i_gry)]
        [InlineData(URLS.konsole_i_gry_gadzety_dla_graczy, URLS.baza_konsole_i_gry)]
        [InlineData(URLS.konsole_i_gry_gogle_vr, URLS.baza_konsole_i_gry)]
        [InlineData(URLS.konsole_i_gry_konsole, URLS.baza_konsole_i_gry)]
        [InlineData(URLS.konsole_i_gry_sluchawki_dla_graczy, URLS.baza_konsole_i_gry)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }

        [Theory]
        [InlineData(URLS.m_konsole_i_gry_gry, URLS.baza_konsole_i_gry)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
