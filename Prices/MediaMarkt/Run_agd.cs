﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_agd
    {
        [Theory]
        [InlineData(URLS.agd_akcesoria_agd, URLS.baza_agd)]
        [InlineData(URLS.agd_bestsellery, URLS.baza_agd)]
        [InlineData(URLS.agd_lodowki_i_zamrazarki, URLS.baza_agd)]
        [InlineData(URLS.agd_plyty_grzejne, URLS.baza_agd)]
        [InlineData(URLS.agd_zestawy_zintegrowane, URLS.baza_agd)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }

        [Theory]
        [InlineData(URLS.m_agd_kuchenki_mikrofalowe, URLS.baza_agd)]
        [InlineData(URLS.m_agd_kuchnie, URLS.baza_agd)]
        [InlineData(URLS.m_agd_pralki_i_suszarki, URLS.baza_agd)]
        [InlineData(URLS.m_agd_zmywarki, URLS.baza_agd)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
