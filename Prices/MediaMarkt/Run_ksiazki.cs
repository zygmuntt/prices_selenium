﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_ksiazki
    {
        [Theory]

        [InlineData(URLS.ksiazki_albumy, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_biografie_listy_wspomnienia, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_dla_dzieci, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_dla_mlodziezy, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_dom_i_ogrod, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_edukacja, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_ekonomia_i_biznes, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_encyklopedie_slowniki, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_eseje_publicystyka_literacka, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_fantastyka_sciencie_fiction, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_felietony, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_gry_planszowe, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_historia, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_hobby, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_horror, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_kalendarze, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_komiksy, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_komputery_informatyka, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_ksiazki_audio, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_kulinaria, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_kultura, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_leksykon, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_lektury, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_literatura, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_literatura_faktu_i_reportaz, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_literatura_polska, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_medycyna, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_nowosci, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_opowiadania, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_pedagogika, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_podroze, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_poezje_wiersze_liryka, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_polityka_politologia, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_popularnonaukowa, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_poradniki, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_powiesc, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_przyroda_rosliny_zwierzeta, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_psychologia_socjologia, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_publicystyka, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_religie, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_rozrywka_humor, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_sport, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_sztuka_malarstwo_architektura, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_wychowanie_rodzina, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_wywiady, URLS.baza_ksiazki)]
        [InlineData(URLS.ksiazki_zdrowie_uroda, URLS.baza_ksiazki)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
    }
}
