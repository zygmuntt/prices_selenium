﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_sprzet_biurowy
    {
        [Theory]
        [InlineData(URLS.sprzet_biurowy_bindownice, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_gilotyny_i_trymery, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_kalkulatory, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_kasoterminale, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_laminatory, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_meble_biurowe, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_niszczarki, URLS.baza_sprzet_biurowy)]
        [InlineData(URLS.sprzet_biurowy_skanery, URLS.baza_sprzet_biurowy)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
        [Theory]
        [InlineData(URLS.m_sprzet_biurowy_akcesoria_do_urzadzen_biurowych, URLS.baza_sprzet_biurowy)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
