﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_Komputery_i_tablety
    {
        [Theory]
        [InlineData(URLS.komputery_i_tablety_bestsellery, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_czytniki_e_bookow, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_drukarki_i_urzadzenia_wielofunkcyjne, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_inteligentyn_dom, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_laptopy_laptopy_2_w_1, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_komputery_stacjonarne, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_monitory, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_oprogramowanie, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_sluchawki_komputerowe, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.komputery_i_tablety_tablety_multimedialne, URLS.baza_komputery_i_tablety)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
        [Theory]
        [InlineData(URLS.m_komputery_i_tabletymaterialy_eksploatacyjne_do_drukarek, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.m_komputery_i_tablety_akcesoria_komputerowe, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.m_komputery_i_tablety_czesci_komputerowe, URLS.baza_komputery_i_tablety)]
        [InlineData(URLS.m_komputery_i_tablety_urzadzenia_sieciowe, URLS.baza_komputery_i_tablety)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
