﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_agd_male
    {
        [Theory]
        [InlineData(URLS.agd_male_bestsellery, URLS.baza_agd_male)]
        [InlineData(URLS.agd_male_chemia_gospodarcza, URLS.baza_agd_male)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }

        [Theory]
        [InlineData(URLS.mm_agd_male_kawa_i_inne_napoje_herbaty, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_kawa_i_inne_napoje_kawy, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_kuchnia_gotowanie_artykuly_kuchenne, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_grzejniki, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_maszyny_do_szycia, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_oczyszczacze_parowe, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_odkurzacze, URLS.baza_agd_male)]
        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_wentylatory, URLS.baza_agd_male)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
