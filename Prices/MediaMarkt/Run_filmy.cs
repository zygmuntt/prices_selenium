﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_filmy
    {
        [Theory]
        [InlineData(URLS.filmy_bestsellery, URLS.baza_filmy)]
        [InlineData(URLS.filmy_filmy_blueray_4k, URLS.baza_filmy)]
        [InlineData(URLS.filmy_nowosci, URLS.baza_filmy)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }

        [Theory]
        [InlineData(URLS.m_filmy_blueray, URLS.baza_filmy)]
        [InlineData(URLS.m_filmy_filmy_dvd, URLS.baza_filmy)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
