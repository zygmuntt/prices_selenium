﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_muzyka2
    {
        [Theory]
        [InlineData(URLS.muzyka_pop, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_nowosci, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_metal, URLS.baza_muzyka)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }

    }
}
