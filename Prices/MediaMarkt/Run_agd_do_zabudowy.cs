﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_agd_do_zabudowy
    {
        [Theory]
        [InlineData(URLS.agd_do_zabudowy_akcesoria_agd, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.agd_do_zabudowy_bestsellery, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.agd_do_zabudowy_ekspresy_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.agd_do_zabudowy_kuchenki_mikrofalowe_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.agd_do_zabudowy_kuchnie_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }



        [Theory]
        [InlineData(URLS.m_agd_do_zabudowy_armatura_kuchenna, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.m_agd_do_zabudowy_lodowki_i_zamrazarki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.m_agd_do_zabudowy_okapy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.m_agd_do_zabudowy_piekarniki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.m_agd_do_zabudowy_plyty_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.m_agd_do_zabudowy_pralki_i_suszarki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        [InlineData(URLS.m_agd_do_zabudowy_zmywarki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }

    }
}
