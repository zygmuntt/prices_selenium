﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_dom_i_ogrod
    {
        [Theory]
        [InlineData(URLS.dom_i_ogrod_elektronarzedzia, URLS.baza_dom_i_ogrod)]
        [InlineData(URLS.dom_i_ogrod_meble_ogrodowe, URLS.baza_dom_i_ogrod)]
        [InlineData(URLS.dom_i_ogrod_myjki_cisnieniowe, URLS.baza_dom_i_ogrod)]
        [InlineData(URLS.dom_i_ogrod_nardzedzia_ogrodowe, URLS.baza_dom_i_ogrod)]
        [InlineData(URLS.dom_i_ogrod_nawadnianie, URLS.baza_dom_i_ogrod)]
        [InlineData(URLS.dom_i_ogrod_nawozy, URLS.baza_dom_i_ogrod)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
    }
}
