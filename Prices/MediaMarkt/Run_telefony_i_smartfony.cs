﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
  public  class Run_telefony_i_smartfony
    {
        [Theory]
        [InlineData(URLS.telefony_i_smartfony_bestsellery, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_faksy, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_krotkofalowki, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_sluchawki_do_telefonu, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_smartfony, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_smartwatche, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_telefonia_internetowa, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_telefony_komorkowe, URLS.baza_telefony_i_smartfony)]
        [InlineData(URLS.telefony_i_smartfony_telefony_stacjonarne, URLS.baza_telefony_i_smartfony)]

        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
        [Theory]
        [InlineData(URLS.m_telefony_i_smartfony_akcesoria, URLS.baza_telefony_i_smartfony)]
        public void multiple_run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Multiple_check(url, baza);
        }
    }
}
