﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
   public class URLS
    {
        //databases
        public const string baza_telefony_i_smartfony = "[dbo].[MediaMarkt_Telefony_i_Smartfony]";
        public const string baza_sprzet_biurowy = "[dbo].[MediaMarkt_Sprzet_biurowy]";
        public const string baza_sport_i_rekreacja = "[dbo].[MediaMarkt_Sport_i_Rekreacja]";
        public const string baza_rtv_i_telewizory = "[dbo].[MediaMarkt_RTV_i_Telewizory]";
        public const string baza_oswietlenie = "[dbo].[MediaMarkt_Oswietlenie]";
        public const string baza_muzyka = "[dbo].[MediaMarkt_Muzyka]";
        public const string baza_ksiazki = "[dbo].[MediaMarkt_Ksiazki]";
        public const string baza_konsole_i_gry = "[dbo].[MediaMarkt_Konsole_i_Gry]";
        public const string baza_komputery_i_tablety = "[dbo].[MediaMarkt_Komputery_i_Tablety]";
        public const string baza_foto_i_kamery = "[dbo].[MediaMarkt_Foto_i_Kamery]";
        public const string baza_filmy = "[dbo].[MediaMarkt_Filmy]";
        public const string baza_dom_i_ogrod = "[dbo].[MediaMarkt_Dom_i_Ogrod]";
        public const string baza_dla_dzieci = "[dbo].[MediaMarkt_Dla_dzieci]";
        public const string baza_agd_male = "[dbo].[MediaMarkt_AGD_male]";
        public const string baza_agd_do_zabudowy = "[dbo].[MediaMarkt_AGD_do_Zabudowy]";
        public const string baza_agd = "[dbo].[MediaMarkt_AGD]";
        //urls
        //telefony_i_smartfony
        public const string telefony_i_smartfony_telefony_stacjonarne = "https://mediamarkt.pl/telefony-i-smartfony/telefony-stacjonarne?limit=100";
        public const string telefony_i_smartfony_telefony_komorkowe = "https://mediamarkt.pl/telefony-i-smartfony/telefony-komorkowe?limit=100";
        public const string telefony_i_smartfony_telefonia_internetowa = "https://mediamarkt.pl/telefony-i-smartfony/telefonia-internetowa?limit=40";
        public const string telefony_i_smartfony_smartfony = "https://mediamarkt.pl/telefony-i-smartfony/smartfony?limit=100";
        public const string telefony_i_smartfony_smartwatche = "https://mediamarkt.pl/telefony-i-smartfony/smartwatches?limit=100";
        public const string telefony_i_smartfony_sluchawki_do_telefonu = "https://mediamarkt.pl/telefony-i-smartfony/sluchawki-do-telefonu?limit=100";
        public const string telefony_i_smartfony_krotkofalowki = "https://mediamarkt.pl/telefony-i-smartfony/krotkofalowki?limit=60";
        public const string telefony_i_smartfony_faksy = "https://mediamarkt.pl/telefony-i-smartfony/faksy?limit=40";
        public const string telefony_i_smartfony_bestsellery= "https://mediamarkt.pl/telefony-i-smartfony/bestsellery?limit=100";
        public const string m_telefony_i_smartfony_akcesoria = "https://mediamarkt.pl/telefony-i-smartfony/akcesoria";
        //sprzet biurowy
        public const string sprzet_biurowy_skanery = "https://mediamarkt.pl/sprzet-biurowy/skanery?limit=50";
        public const string sprzet_biurowy_niszczarki = "https://mediamarkt.pl/sprzet-biurowy/niszczarki?limit=80";
        public const string sprzet_biurowy_meble_biurowe = "https://mediamarkt.pl/sprzet-biurowy/meble-biurowe?limit=50";
        public const string sprzet_biurowy_laminatory = "https://mediamarkt.pl/sprzet-biurowy/laminatory?limit=50";
        public const string sprzet_biurowy_kasoterminale = "https://mediamarkt.pl/sprzet-biurowy/kasoterminale";
        public const string sprzet_biurowy_kalkulatory = "https://mediamarkt.pl/sprzet-biurowy/kalkulatory?limit=100";
        public const string sprzet_biurowy_gilotyny_i_trymery = "https://mediamarkt.pl/sprzet-biurowy/gilotyny-i-trymery?limit=50";
        public const string sprzet_biurowy_bindownice = "https://mediamarkt.pl/sprzet-biurowy/bindownice";
        public const string m_sprzet_biurowy_akcesoria_do_urzadzen_biurowych = "https://mediamarkt.pl/sprzet-biurowy/akcesoria-do-urzadzen-biurowych";
        //sport i rekreacja
        public const string sport_i_rekreacja_akcesoria = "https://mediamarkt.pl/sport-i-rekreacja/akcesoria?limit=100";
        public const string sport_i_rekreacja_drony = "https://mediamarkt.pl/sport-i-rekreacja/drony?limit=100";
        public const string sport_i_rekreacja_elektryczne_deskorolki_i_hulajnogi = "https://mediamarkt.pl/sport-i-rekreacja/elektryczne-deskorolki?limit=100";
        public const string sport_i_rekreacja_etui_opaski_na_ramie = "https://mediamarkt.pl/sport-i-rekreacja/etui-opaski-na-ramie?limit=100";
        public const string sport_i_rekreacja_kamery_sportowe = "https://mediamarkt.pl/sport-i-rekreacja/kamery-sportowe?limit=100";
        public const string sport_i_rekreacja_outdoor = "https://mediamarkt.pl/sport-i-rekreacja/outdoor";
        public const string sport_i_rekreacja_rowery_elektryczne = "https://mediamarkt.pl/sport-i-rekreacja/rowery-elektryczne";
        public const string sport_i_rekreacja_skating = "https://mediamarkt.pl/sport-i-rekreacja/skating?limit=100";
        public const string sport_i_rekreacja_sluchawki_sportowe = "https://mediamarkt.pl/sport-i-rekreacja/sluchawki-sportowe?limit=100";
        public const string sport_i_rekreacja_smartwache_i_opaski = "https://mediamarkt.pl/sport-i-rekreacja/smartwatche-i-opaski?limit=100";
        public const string sport_i_rekreacja_urzadzenia_treningowe = "https://mediamarkt.pl/sport-i-rekreacja/urzadzenia-treningowe?limit=100";
        public const string sport_i_rekreacja_zegarki_sportowe = "https://mediamarkt.pl/sport-i-rekreacja/zegarki-sportowe?limit=100";
        //rtv i telewizory
        public const string rtv_i_telewizory_akcesoria_audio_video = "https://mediamarkt.pl/rtv-i-telewizory/akcesoria-audio-video?limit=100";
        public const string rtv_i_telewizory_bestsellery = "https://mediamarkt.pl/rtv-i-telewizory/bestsellery?limit=100";
        public const string rtv_i_telewizory_car_audio_i_nawigacja = "https://mediamarkt.pl/rtv-i-telewizory/car-audio-i-nawigacja?limit=100";
        public const string rtv_i_telewizory_drobny_sprzet_audio_i_video = "https://mediamarkt.pl/rtv-i-telewizory/drobny-sprzet-audio-i-video?limit=100";
        public const string rtv_i_telewizory_hifi_audio = "https://mediamarkt.pl/rtv-i-telewizory/hifi-audio?limit=100";
        public const string rtv_i_telewizory_kino_domowe = "https://mediamarkt.pl/rtv-i-telewizory/kino-domowe?limit=100";
        public const string rtv_i_telewizory_projektory = "https://mediamarkt.pl/rtv-i-telewizory/projektory?limit=100";
        public const string rtv_i_telewizory_sluchawki_audio = "https://mediamarkt.pl/rtv-i-telewizory/sluchawki-audio?limit=100";
        public const string rtv_i_telewizory_telewizory = "https://mediamarkt.pl/rtv-i-telewizory/telewizory?limit=100";
        public const string rtv_i_telewizory_tv_satelitarna_i_naziemna = "https://mediamarkt.pl/rtv-i-telewizory/tv-satelitarna-i-naziemna?limit=100";
       
        //oswietlenie
        public const string oswietlenie_akcesoria_oswietleniowe = "https://mediamarkt.pl/oswietlenie/akcesoria-oswietleniowe?limit=50";
        public const string oswietlenie_czujniki_i_alarmy = " https://mediamarkt.pl/oswietlenie/czujniki-i-alarmy?limit=100";
        public const string oswietlenie_lamy_pozostale = "https://mediamarkt.pl/oswietlenie/lampy-dekoracyjne?limit=100";
        public const string oswietlenie_lamy_stolowe_i_biurkowe = " https://mediamarkt.pl/oswietlenie/lampy-stolowe-i-biurkowe?limit=100";
        public const string oswietlenie_mierniki_i_przyrzady_pomiarowe = "https://mediamarkt.pl/oswietlenie/mierniki-energii?limit=50";
        public const string oswietlenie__oswietlenie_dekoracyjne_i_nastrojowe = "https://mediamarkt.pl/oswietlenie/oswietlenie-dekoracyjne-i-nastrojowe?limit=100";
        public const string oswietlenie_rozgalezniki_elektrycze = "https://mediamarkt.pl/oswietlenie/rozgalezniki-elektryczne?limit=50";
        public const string oswietlenie_swietlowki_kompaktowe = "https://mediamarkt.pl/oswietlenie/swietlowki-kompaktowe?limit=50";
        public const string oswietlenie_tasmy_led = "https://mediamarkt.pl/oswietlenie/tasmy-led?limit=50";
        public const string oswietlenie_zerowki_halogenowe = "https://mediamarkt.pl/oswietlenie/zarowki-halogenowe?limit=100";
        public const string oswietlenie_zarowki_led = "https://mediamarkt.pl/oswietlenie/zarowki-led?limit=100";
        public const string oswietlenie_zarowki_tradycyjne = "https://mediamarkt.pl/oswietlenie/zarowki-tradycyjne?limit=50";
        public const string oswietlenie_lampy_przenosne = "https://mediamarkt.pl/oswietlenie/latarki-i-lampy-przenosne?limit=100";
        //muzyka
        public const string muzyka_bestsellery = "https://mediamarkt.pl/muzyka/bestsellery?limit=100";
        public const string muzyka_country_folk_gospel = " https://mediamarkt.pl/muzyka/country-folk-gospel?limit=100";
        public const string muzyka_gadzety = "https://mediamarkt.pl/muzyka/gadzety?limit=100";
        public const string muzyka_hiphop_rap = "https://mediamarkt.pl/muzyka/hip-hop-soul-rap-rb?limit=100";
        public const string muzyka_jazz_blues = "https://mediamarkt.pl/muzyka/jazz-blues?limit=100";
        public const string muzyka_metal = "https://mediamarkt.pl/muzyka/metal?limit=100";
        public const string muzyka_dla_dzieci_i_mlodziezy = "https://mediamarkt.pl/muzyka/muzyka-dla-dzieci-mlodziezy?limit=100";
        public const string muzyka_dvd_bluray = "https://mediamarkt.pl/muzyka/muzyka-dvd-bluray?limit=100";
        public const string muzyka_klasyczna = "https://mediamarkt.pl/muzyka/muzyka-klasyczna?limit=100";
        public const string muzyka_non_ficition = "https://mediamarkt.pl/muzyka/non-fiction?limit=100";
        public const string muzyka_nowosci = "https://mediamarkt.pl/muzyka/nowosci?limit=100";
        public const string muzyka_plyty_winylowe = "https://mediamarkt.pl/muzyka/plyty-winylowe?limit=100";
        public const string muzyka_pop = "https://mediamarkt.pl/muzyka/pop?limit=100";
        public const string muzyka_reggae_ska = "https://mediamarkt.pl/muzyka/reggae-ska?limit=100";
        public const string muzyka_rock_alternatywa = "https://mediamarkt.pl/muzyka/rock-alternatywa?limit=100";
        public const string muzyka_soundtrack_muzyka_filmowa = "https://mediamarkt.pl/muzyka/soundtrack-muzyka-filmowa?limit=100";
        public const string muzyka_techno_disco_dance = "https://mediamarkt.pl/muzyka/techno-disco-dance?limit=100";
        public const string muzyka_world_music = "https://mediamarkt.pl/muzyka/world-music?limit=100";
        //ksiazki
        public const string ksiazki_albumy = "https://mediamarkt.pl/ksiazki/albumy?limit=100";
        public const string ksiazki_biografie_listy_wspomnienia = "https://mediamarkt.pl/ksiazki/biografie-listy-wspomnienia?limit=100";
        public const string ksiazki_dla_dzieci = "https://mediamarkt.pl/ksiazki/dla-dzieci?limit=100";
        public const string ksiazki_dla_mlodziezy = "https://mediamarkt.pl/ksiazki/dla-mlodziezy?limit=100";
        public const string ksiazki_dom_i_ogrod = "https://mediamarkt.pl/ksiazki/dom-i-ogrod?limit=50";
        public const string ksiazki_edukacja = "https://mediamarkt.pl/ksiazki/edukacja?limit=100";
        public const string ksiazki_ekonomia_i_biznes = "https://mediamarkt.pl/ksiazki/ekonomia-i-biznes?limit=50";
        public const string ksiazki_eseje_publicystyka_literacka = "https://mediamarkt.pl/ksiazki/eseje-publicystyka-literacka?limit=50";
        public const string ksiazki_encyklopedie_slowniki = "https://mediamarkt.pl/ksiazki/encyklopedie-slowniki?limit=100";
        public const string ksiazki_fantastyka_sciencie_fiction = "https://mediamarkt.pl/ksiazki/fantastyka-fantasy-science-fiction?limit=100";
        public const string ksiazki_felietony = "https://mediamarkt.pl/ksiazki/felietony?limit=50";
        public const string ksiazki_gry_planszowe = "https://mediamarkt.pl/ksiazki/gry-planszowe?limit=100";
        public const string ksiazki_historia = "https://mediamarkt.pl/ksiazki/historia?limit=100";
        public const string ksiazki_hobby = "https://mediamarkt.pl/ksiazki/hobby?limit=50";
        public const string ksiazki_horror = "https://mediamarkt.pl/ksiazki/horror?limit=50";
        public const string ksiazki_humanistyka = "https://mediamarkt.pl/ksiazki/humanistyka?limit=50";
        public const string ksiazki_kalendarze = " https://mediamarkt.pl/ksiazki/kalendarze?limit=50";
        public const string ksiazki_komiksy = "https://mediamarkt.pl/ksiazki/komiksy?limit=100";
        public const string ksiazki_komputery_informatyka = "https://mediamarkt.pl/ksiazki/komputery-informatyka?limit=100";
        public const string ksiazki_ksiazki_audio = "https://mediamarkt.pl/ksiazki/ksiazki-audio?limit=100";
        public const string ksiazki_kulinaria = "https://mediamarkt.pl/ksiazki/kulinaria?limit=100";
        public const string ksiazki_kultura = " https://mediamarkt.pl/ksiazki/kultura?limit=50";
        public const string ksiazki_leksykon = "https://mediamarkt.pl/ksiazki/leksykon?limit=50";
        public const string ksiazki_lektury = "https://mediamarkt.pl/ksiazki/lektury?limit=100";
        public const string ksiazki_literatura = "https://mediamarkt.pl/ksiazki/literatura?limit=100";
        public const string ksiazki_literatura_faktu_i_reportaz = "https://mediamarkt.pl/ksiazki/literatura-faktu-i-reportaz?limit=100";
        public const string ksiazki_literatura_polska = "https://mediamarkt.pl/ksiazki/literatura-polska?limit=50";
        public const string ksiazki_medycyna = "https://mediamarkt.pl/ksiazki/medycyna?limit=50";
        public const string ksiazki_nowosci = "https://mediamarkt.pl/ksiazki/nowosci?limit=100";
        public const string ksiazki_opowiadania = "https://mediamarkt.pl/ksiazki/opowiadania?limit=100";
        public const string ksiazki_pedagogika = "https://mediamarkt.pl/ksiazki/pedagogika?limit=50";
        public const string ksiazki_podroze = " https://mediamarkt.pl/ksiazki/podroze?limit=100";
        public const string ksiazki_poezje_wiersze_liryka = "https://mediamarkt.pl/ksiazki/poezja-wiersze-liryka?limit=50";
        public const string ksiazki_polityka_politologia = "https://mediamarkt.pl/ksiazki/polityka-politologia?limit=50";
        public const string ksiazki_popularnonaukowa = "https://mediamarkt.pl/ksiazki/popularnonaukowa?limit=50";
        public const string ksiazki_poradniki = "https://mediamarkt.pl/ksiazki/poradniki?limit=100";
        public const string ksiazki_powiesc = "https://mediamarkt.pl/ksiazki/powiesc?limit=100";
        public const string ksiazki_przyroda_rosliny_zwierzeta = "https://mediamarkt.pl/ksiazki/przyroda-rosliny-zwierzeta?limit=50";
        public const string ksiazki_psychologia_socjologia = "https://mediamarkt.pl/ksiazki/psychologia-socjologia?limit=50";
        public const string ksiazki_publicystyka = "https://mediamarkt.pl/ksiazki/publicystyka?limit=50";
        public const string ksiazki_religie = "https://mediamarkt.pl/ksiazki/religie?limit=50";
        public const string ksiazki_rozrywka_humor = "https://mediamarkt.pl/ksiazki/rozrywka-humor?limit=50";
        public const string ksiazki_sport = "https://mediamarkt.pl/ksiazki/sport?limit=50";
        public const string ksiazki_sztuka_malarstwo_architektura = "https://mediamarkt.pl/ksiazki/sztuka-malarstwo-architektura?limit=50";
        public const string ksiazki_wychowanie_rodzina = "https://mediamarkt.pl/ksiazki/wychowanie-rodzina?limit=50";
        public const string ksiazki_wywiady = "https://mediamarkt.pl/ksiazki/wywiady?limit=50";
        public const string ksiazki_zdrowie_uroda = "https://mediamarkt.pl/ksiazki/zdrowie-uroda?limit=50";
        //konsole_i_gry
        public const string konsole_i_gry_bestsellery = "https://mediamarkt.pl/konsole-i-gry/bestsellery?limit=100";
        public const string konsole_i_gry_akcesoria_do_konsol = "https://mediamarkt.pl/konsole-i-gry/akcesoria-do-konsol?limit=100";
        public const string konsole_i_gry_gadzety_dla_graczy = "https://mediamarkt.pl/konsole-i-gry/gadzety-dla-graczy?limit=100";
        public const string konsole_i_gry_gogle_vr = "https://mediamarkt.pl/konsole-i-gry/gogle-vr?limit=100";
        public const string m_konsole_i_gry_gry = "https://mediamarkt.pl/konsole-i-gry/gry";
        public const string konsole_i_gry_konsole = "https://mediamarkt.pl/konsole-i-gry/konsole?limit=100";
        public const string konsole_i_gry_sluchawki_dla_graczy = " https://mediamarkt.pl/konsole-i-gry/sluchawki-dla-graczy?limit=100";
        //komputery_i_tablety
        public const string m_komputery_i_tablety_akcesoria_komputerowe = "https://mediamarkt.pl/komputery-i-tablety/akcesoria-komputerowe";
        public const string komputery_i_tablety_bestsellery = "https://mediamarkt.pl/komputery-i-tablety/bestsellery?limit=100";
        public const string m_komputery_i_tablety_czesci_komputerowe = "https://mediamarkt.pl/komputery-i-tablety/czesci-komputerowe";
        public const string komputery_i_tablety_czytniki_e_bookow = "https://mediamarkt.pl/komputery-i-tablety/czytniki-e-bookow?limit=80";
        public const string komputery_i_tablety_drukarki_i_urzadzenia_wielofunkcyjne = "https://mediamarkt.pl/komputery-i-tablety/drukarki-i-urzadzenia-wielofunkcyjne?limit=100";
        public const string komputery_i_tablety_inteligentyn_dom = "https://mediamarkt.pl/komputery-i-tablety/inteligentny-dom?limit=100";
        public const string komputery_i_tablety_komputery_stacjonarne = "https://mediamarkt.pl/komputery-i-tablety/komputery-stacjonarne?limit=100";
        public const string komputery_i_tablety_laptopy_laptopy_2_w_1 = "https://mediamarkt.pl/komputery-i-tablety/laptopy-laptopy-2-w-1?limit=100";
        public const string m_komputery_i_tabletymaterialy_eksploatacyjne_do_drukarek = "https://mediamarkt.pl/komputery-i-tablety/materialy-eksploatacyjne-do-drukarek";
        public const string komputery_i_tablety_monitory = "https://mediamarkt.pl/komputery-i-tablety/monitory-led?limit=100";
        public const string komputery_i_tablety_oprogramowanie = "https://mediamarkt.pl/komputery-i-tablety/oprogramowanie?limit=100";
        public const string komputery_i_tablety_sluchawki_komputerowe = "https://mediamarkt.pl/komputery-i-tablety/sluchawki-komputerowe?limit=100";
        public const string komputery_i_tablety_tablety_multimedialne = "https://mediamarkt.pl/komputery-i-tablety/tablety-multimedialne?limit=100";
        public const string m_komputery_i_tablety_urzadzenia_sieciowe = "https://mediamarkt.pl/komputery-i-tablety/urzadzenia-sieciowe";
        //foto i kamery
        public const string foto_i_kamery_akcesoria_fotograficzne = " https://mediamarkt.pl/foto-i-kamery/akcesoria-fotograficzne?limit=100";
        public const string foto_i_kamery_aparaty_cyfrowe = "https://mediamarkt.pl/foto-i-kamery/aparaty-cyfrowe?limit=100";
        public const string foto_i_kamery_bestsellery = "https://mediamarkt.pl/foto-i-kamery/bestsellery?limit=100";
        public const string foto_i_kamery_drony = "https://mediamarkt.pl/foto-i-kamery/drony?limit=100";
        public const string foto_i_kamery_drukarki_fotograficzne = "https://mediamarkt.pl/foto-i-kamery/drukarki-fotograficzne?limit=70";
        public const string foto_i_kamery_kamery = "https://mediamarkt.pl/foto-i-kamery/kamery?limit=100";
        public const string foto_i_kamery_lornetki_i_lunety = "https://mediamarkt.pl/foto-i-kamery/lornetki-i-lunety?limit=100";
        public const string foto_i_kamery_obiektywy_i_akcesoria = "https://mediamarkt.pl/foto-i-kamery/obiektywy-i-akcesoria?limit=100";
        public const string foto_i_kamery_ramki_cyfrowe = "https://mediamarkt.pl/foto-i-kamery/ramki-cyfrowe?limit=70";
        //filmy
        public const string filmy_bestsellery = "https://mediamarkt.pl/filmy/bestsellery?limit=100";
        public const string filmy_filmy_blueray_4k = "https://mediamarkt.pl/filmy/filmy-blu-ray-4filmy-blu-ray-4kk?limit=100";
        public const string m_filmy_filmy_dvd = "https://mediamarkt.pl/filmy/filmy-dvd";
        public const string filmy_nowosci = "https://mediamarkt.pl/filmy/nowosci?limit=100";
        public const string m_filmy_blueray = "https://mediamarkt.pl/filmy/filmy-blu-ray";
        //dom_i_ogrod
        public const string dom_i_ogrod_elektronarzedzia = "https://mediamarkt.pl/dom-i-ogrod/elektronarzedzia?limit=100";
        public const string dom_i_ogrod_meble_ogrodowe = "https://mediamarkt.pl/dom-i-ogrod/meble-ogrodowe?limit=100";
        public const string dom_i_ogrod_myjki_cisnieniowe = "https://mediamarkt.pl/dom-i-ogrod/myjki-cisnieniowe?limit=100";
        public const string dom_i_ogrod_nardzedzia_ogrodowe = "https://mediamarkt.pl/dom-i-ogrod/narzedzia-ogrodowe?limit=100";
        public const string dom_i_ogrod_nawozy = "https://mediamarkt.pl/dom-i-ogrod/nawozy?limit=100";
        public const string dom_i_ogrod_nawadnianie = "https://mediamarkt.pl/dom-i-ogrod/nawadnianie?limit=100";
        //dla dzieci
        public const string dla_dzieci_artykuly_szkolne = "https://mediamarkt.pl/dla-dzieci/artykuly-szkolne?limit=40";
        public const string dla_dzieci_opieka_i_higiena = "https://mediamarkt.pl/dla-dzieci/opieka-i-higiena?limit=100";
        public const string dla_dzieci_zabawki = "https://mediamarkt.pl/dla-dzieci/zabawki?limit=100";
        //agd male
        public const string agd_male_bestsellery = "https://mediamarkt.pl/agd-male/bestsellery?limit=100";
        public const string agd_male_chemia_gospodarcza = "https://mediamarkt.pl/agd-male/chemia-gospodarcza?limit=100";
        public const string m_agd_male_dom_czystosc_i_porzadek = "https://mediamarkt.pl/agd-male/dom-czystosc-i-porzadek";
        public const string m_agd_male_ekspresy_do_kawy = "https://mediamarkt.pl/agd-male/ekspresy";
        public const string m_agd_male_kawa_i_inne_napoje = "https://mediamarkt.pl/agd-male/kawa-i-inne-napoje";
        public const string m_agd_male_kuchnia_gotowanie_i_przygotowanie_potraw = "https://mediamarkt.pl/agd-male/kuchnia-gotowanie-i-przygotowywanie-potraw";
        public const string m_agd_male_oczyszczacze_i_nawlizacze_powietrza = "https://mediamarkt.pl/agd-male/oczyszczacze-i-nawilzacze-powietrza";
        public const string m_agd_male_odkurzacze = "https://mediamarkt.pl/agd-male/odkurzacze";
        public const string m_agd_male_uroda_pielegnacja_i_zdrowie = "https://mediamarkt.pl/agd-male/uroda-pielegnacja-i-zdrowie";
        public const string m_agd_male_uzdatnianie_powietrza = "https://mediamarkt.pl/agd-male/uzdatnianie-powietrza";
        public const string mm_agd_male_uzdatnianie_powietrza_grzejniki = "https://mediamarkt.pl/agd-male/uzdatnianie-powietrza/grzejniki";
        public const string mm_agd_male_uzdatnianie_powietrza_wentylatory = "https://mediamarkt.pl/agd-male/uzdatnianie-powietrza/wentylatory";
        public const string mm_agd_male_uzdatnianie_powietrza_odkurzacze ="https://mediamarkt.pl/agd-male/dom-czystosc-i-porzadek/odkurzacze";
        public const string mm_agd_male_uzdatnianie_powietrza_maszyny_do_szycia = "https://mediamarkt.pl/agd-male/dom-czystosc-i-porzadek/maszyny-do-szycia-i-owerloki";
        public const string mm_agd_male_uzdatnianie_powietrza_oczyszczacze_parowe = "https://mediamarkt.pl/agd-male/dom-czystosc-i-porzadek/oczyszczacze-parowe-i-akcesoria";
        public const string mm_agd_male_kuchnia_gotowanie_artykuly_kuchenne = "https://mediamarkt.pl/agd-male/kuchnia-gotowanie-i-przygotowywanie-potraw/artykuly-kuchenne";
        public const string mm_agd_male_kawa_i_inne_napoje_herbaty = "https://mediamarkt.pl/agd-male/kawa-i-inne-napoje/herbaty";
        public const string mm_agd_male_kawa_i_inne_napoje_kawy = "https://mediamarkt.pl/agd-male/kawa-i-inne-napoje/kawy";
        //agd do zabudowy
        public const string agd_do_zabudowy_akcesoria_agd = "https://mediamarkt.pl/agd-do-zabudowy/akcesoria-do-kuchni?limit=100";
        public const string m_agd_do_zabudowy_armatura_kuchenna = "https://mediamarkt.pl/agd-do-zabudowy/armatura-kuchenna";
        public const string agd_do_zabudowy_bestsellery = "https://mediamarkt.pl/agd-do-zabudowy/bestsellery?limit=100";
        public const string agd_do_zabudowy_ekspresy_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/ekspresy-do-zabudowy?limit=40";
        public const string agd_do_zabudowy_kuchenki_mikrofalowe_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/kuchenki-mikrofalowe-do-zabudowy?limit=100";
        public const string agd_do_zabudowy_kuchnie_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/kuchnie-do-zabudowy/akcesoria-do-kuchni?limit=90";
        public const string m_agd_do_zabudowy_lodowki_i_zamrazarki_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/lodowki-i-zamrazarki-do-zabudowy";
        public const string m_agd_do_zabudowy_okapy = "https://mediamarkt.pl/agd-do-zabudowy/okapy";
        public const string m_agd_do_zabudowy_piekarniki_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/piekarniki-do-zabudowy";
        public const string m_agd_do_zabudowy_plyty_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/plyty-do-zabudowy";
        public const string m_agd_do_zabudowy_pralki_i_suszarki_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/pralki-i-suszarki-do-zabudowy";
        public const string m_agd_do_zabudowy_zmywarki_do_zabudowy = "https://mediamarkt.pl/agd-do-zabudowy/zmywarki-do-zabudowy";
        //agd
        public const string agd_akcesoria_agd = "https://mediamarkt.pl/agd/akcesoria-agd?limit=100";
        public const string agd_bestsellery = "https://mediamarkt.pl/agd/bestsellery?limit=100";
        public const string m_agd_kuchenki_mikrofalowe = "https://mediamarkt.pl/agd/kuchenki-mikrofalowe";
        public const string m_agd_kuchnie = "https://mediamarkt.pl/agd/kuchnie";
        public const string agd_lodowki_i_zamrazarki = "https://mediamarkt.pl/agd/lodowki-i-zamrazarki?limit=100";
        public const string agd_plyty_grzejne = "https://mediamarkt.pl/agd/plyty-grzejne?limit=80";
        public const string m_agd_pralki_i_suszarki = "https://mediamarkt.pl/agd/pralki-i-suszarki";
        public const string agd_zestawy_zintegrowane = " https://mediamarkt.pl/agd/zestawy-zintegrowane?limit=40";
        public const string m_agd_zmywarki = "https://mediamarkt.pl/agd/zmywarki";




    }
}
