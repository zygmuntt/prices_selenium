﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_oswietlenie
    {
        [Theory]
        [InlineData(URLS.oswietlenie_akcesoria_oswietleniowe, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_czujniki_i_alarmy, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_lampy_przenosne, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_lamy_pozostale, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_lamy_stolowe_i_biurkowe, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_mierniki_i_przyrzady_pomiarowe, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_rozgalezniki_elektrycze, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_swietlowki_kompaktowe, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_tasmy_led, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_zarowki_led, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_zarowki_tradycyjne, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie_zerowki_halogenowe, URLS.baza_oswietlenie)]
        [InlineData(URLS.oswietlenie__oswietlenie_dekoracyjne_i_nastrojowe, URLS.baza_oswietlenie)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
    }
}
