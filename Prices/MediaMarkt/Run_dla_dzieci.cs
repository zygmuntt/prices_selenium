﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_dla_dzieci
    {
        [Theory]
        [InlineData(URLS.dla_dzieci_artykuly_szkolne, URLS.baza_dla_dzieci)]
        [InlineData(URLS.dla_dzieci_opieka_i_higiena, URLS.baza_dla_dzieci)]
        [InlineData(URLS.dla_dzieci_zabawki, URLS.baza_dla_dzieci)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }


    }
}
