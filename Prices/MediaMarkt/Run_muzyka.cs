﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_muzyka
    {
        [Theory]
        [InlineData(URLS.muzyka_bestsellery, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_country_folk_gospel, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_dla_dzieci_i_mlodziezy, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_dvd_bluray, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_gadzety, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_hiphop_rap, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_jazz_blues, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_klasyczna, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_non_ficition, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_plyty_winylowe, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_reggae_ska, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_rock_alternatywa, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_soundtrack_muzyka_filmowa, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_techno_disco_dance, URLS.baza_muzyka)]
        [InlineData(URLS.muzyka_world_music, URLS.baza_muzyka)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
    }
}
