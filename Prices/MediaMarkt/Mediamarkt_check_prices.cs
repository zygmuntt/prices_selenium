﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Mediamarkt_check_prices
    {
        
            private IWebDriver driver;
            public void Check(string url, string bazadanych)
            {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                string abc = "//a[starts-with(@href,'/cart/add?id=')]";
                var pages = driver.FindElement(By.CssSelector(".b-listing_toolBarPagination > nav:nth-child(1) > span:nth-child(3)")).Text;
                string abcd = pages.Replace("z ", "");
                int pages_int = Convert.ToInt32(abcd);
                int i = 0;
                do

                {
                    IList<IWebElement> all = driver.FindElements(By.XPath(abc));
                    foreach (IWebElement element in all)
                    {
                        var id = element.GetAttribute("data-offer-id").Replace("'", "");
                        var name = element.GetAttribute("data-offer-name").Replace("'", "");
                        var price = element.GetAttribute("data-offer-price").Replace("'", "");
                    AGAIN:
                    try
                    {
                        SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, Price_MediaMarkt, MediaMarkt_Local_id) values('{name}', '{price}', '{id}') else update prices.dbo.Articles set Price_MediaMarkt = '{price}', MediaMarkt_Local_id = '{id}' where name = '{name}'");
                    }
                    catch (System.Data.SqlClient.SqlException deadlock)
                    {
                        if (deadlock.Number == 1205)
                            goto AGAIN;
                    }
                    AGAIN2:
                    try
                    {
                        SQL.Insert($"begin if not exists (select* from [prices].[dbo].[MediaMarkt] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[MediaMarkt] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                    }
                    catch (System.Data.SqlClient.SqlException deadlock)
                    {
                        if (deadlock.Number == 1205)
                            goto AGAIN2;
                    }
                }
                    i++;
                    try
                    {
                        var next = driver.FindElement(By.CssSelector(".b-listing_toolBarPagination > nav:nth-child(1) > a:nth-child(4) > span:nth-child(2) > svg:nth-child(1)"));
                        next.Click();
                    }
                    catch
                    { }
                }
                while (i < pages_int);
                driver.Quit();
            }

        public void Multiple_check(string url, string bazadanych)
        {

            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            IList<IWebElement> categories = driver.FindElements(By.ClassName("m-tiles_picture"));
            IList<IWebElement> list_of_categoires = new List<IWebElement>();
            for (int j = 0; j < categories.Count; j++)
            {
                categories = driver.FindElements(By.ClassName("m-tiles_picture"));
                categories[j].Click();

                string abc = "//a[starts-with(@href,'/cart/add?id=')]";
                try {
                    var pages = driver.FindElement(By.CssSelector(".b-listing_toolBarPagination > nav:nth-child(1) > span:nth-child(3)")).Text;
                    string abcd = pages.Replace("z ", "");
                    int pages_int = Convert.ToInt32(abcd);
                    int i = 0;
                    do

                    {
                        IList<IWebElement> all = driver.FindElements(By.XPath(abc));
                        foreach (IWebElement element2 in all)
                        {
                            var id = element2.GetAttribute("data-offer-id").Replace("'", "");
                            var name = element2.GetAttribute("data-offer-name").Replace("'", "");
                            var price = element2.GetAttribute("data-offer-price").Replace("'", "");
                            AGAIN:
                            try
                            {
                                SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, Price_MediaMarkt, MediaMarkt_Local_id) values('{name}', '{price}', '{id}') else update prices.dbo.Articles set Price_MediaMarkt = '{price}', MediaMarkt_Local_id = '{id}' where name = '{name}'");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN;
                            }
                            AGAIN2:
                            try
                            {
                                SQL.Insert($"begin if not exists (select* from [prices].[dbo].[MediaMarkt] where price = '{price}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[MediaMarkt] (local_id,price,date,name) values ('{id}','{price}','{time}','{name}') end end ");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN2;
                            }
                        }
                        i++;

                    
                    try
                    {
                        var next = driver.FindElement(By.CssSelector(".b-listing_toolBarPagination > nav:nth-child(1) > a:nth-child(4) > span:nth-child(2) > svg:nth-child(1)"));
                        next.Click();
                    }
                    catch
                    { }
                }
                while (i < pages_int) ;
            }
            catch { }
                driver.Url = url;
            }


            driver.Quit();
        }






    }
}
