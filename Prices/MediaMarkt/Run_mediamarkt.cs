﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using OpenQA.Selenium.Support.PageObjects;
//using System.Threading;
//using Xunit;
//using OpenQA.Selenium.Chrome;

//namespace Prices.MediaMarkt
//{
//    public class Run_mediamarkt
//    {
//        [Theory]
//        //agd
//        [InlineData(URLS.agd_akcesoria_agd, URLS.baza_agd)]
//        [InlineData(URLS.agd_bestsellery, URLS.baza_agd)]
//        [InlineData(URLS.agd_lodowki_i_zamrazarki, URLS.baza_agd)]
//        [InlineData(URLS.agd_plyty_grzejne, URLS.baza_agd)]
//        [InlineData(URLS.agd_zestawy_zintegrowane, URLS.baza_agd)]
//        //agd male
//        [InlineData(URLS.agd_male_bestsellery, URLS.baza_agd_male)]
//        [InlineData(URLS.agd_male_chemia_gospodarcza, URLS.baza_agd_male)]
//        //agd do zabudowy
//        [InlineData(URLS.agd_do_zabudowy_akcesoria_agd, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.agd_do_zabudowy_bestsellery, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.agd_do_zabudowy_ekspresy_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.agd_do_zabudowy_kuchenki_mikrofalowe_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.agd_do_zabudowy_kuchnie_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        //dla dzieci
//        [InlineData(URLS.dla_dzieci_artykuly_szkolne, URLS.baza_dla_dzieci)]
//        [InlineData(URLS.dla_dzieci_opieka_i_higiena, URLS.baza_dla_dzieci)]
//        [InlineData(URLS.dla_dzieci_zabawki, URLS.baza_dla_dzieci)]
//        //dom i ogrod
//        [InlineData(URLS.dom_i_ogrod_elektronarzedzia, URLS.baza_dom_i_ogrod)]
//        [InlineData(URLS.dom_i_ogrod_meble_ogrodowe, URLS.baza_dom_i_ogrod)]
//        [InlineData(URLS.dom_i_ogrod_myjki_cisnieniowe, URLS.baza_dom_i_ogrod)]
//        [InlineData(URLS.dom_i_ogrod_nardzedzia_ogrodowe, URLS.baza_dom_i_ogrod)]
//        [InlineData(URLS.dom_i_ogrod_nawadnianie, URLS.baza_dom_i_ogrod)]
//        [InlineData(URLS.dom_i_ogrod_nawozy, URLS.baza_dom_i_ogrod)]
//        //filmy
//        [InlineData(URLS.filmy_bestsellery, URLS.baza_filmy)]
//        [InlineData(URLS.filmy_filmy_blueray_4k, URLS.baza_filmy)]
//        [InlineData(URLS.filmy_nowosci, URLS.baza_filmy)]
//        //foto i kamery
//        [InlineData(URLS.foto_i_kamery_akcesoria_fotograficzne, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_aparaty_cyfrowe, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_bestsellery, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_drony, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_drukarki_fotograficzne, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_kamery, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_lornetki_i_lunety, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_obiektywy_i_akcesoria, URLS.baza_foto_i_kamery)]
//        [InlineData(URLS.foto_i_kamery_ramki_cyfrowe, URLS.baza_foto_i_kamery)]
//        //komputery i tablety
//        [InlineData(URLS.komputery_i_tablety_bestsellery, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_czytniki_e_bookow, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_drukarki_i_urzadzenia_wielofunkcyjne, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_inteligentyn_dom, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_laptopy_laptopy_2_w_1, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_komputery_stacjonarne, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_monitory, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_oprogramowanie, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_sluchawki_komputerowe, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.komputery_i_tablety_tablety_multimedialne, URLS.baza_komputery_i_tablety)]
//        //konsole i gry
//        [InlineData(URLS.konsole_i_gry_akcesoria_do_konsol, URLS.baza_konsole_i_gry)]
//        [InlineData(URLS.konsole_i_gry_bestsellery, URLS.baza_konsole_i_gry)]
//        [InlineData(URLS.konsole_i_gry_gadzety_dla_graczy, URLS.baza_konsole_i_gry)]
//        [InlineData(URLS.konsole_i_gry_gogle_vr, URLS.baza_konsole_i_gry)]
//        [InlineData(URLS.konsole_i_gry_konsole, URLS.baza_konsole_i_gry)]
//        [InlineData(URLS.konsole_i_gry_sluchawki_dla_graczy, URLS.baza_konsole_i_gry)]
//        //ksiazki 
//        [InlineData(URLS.ksiazki_albumy, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_biografie_listy_wspomnienia, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_dla_dzieci, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_dla_mlodziezy, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_dom_i_ogrod, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_edukacja, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_ekonomia_i_biznes, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_encyklopedie_slowniki, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_eseje_publicystyka_literacka, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_fantastyka_sciencie_fiction, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_felietony, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_gry_planszowe, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_historia, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_hobby, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_horror, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_humanistyka, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_kalendarze, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_komiksy, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_komputery_informatyka, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_ksiazki_audio, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_kulinaria, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_kultura, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_leksykon, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_lektury, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_literatura, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_literatura_faktu_i_reportaz, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_literatura_polska, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_medycyna, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_nowosci, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_opowiadania, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_pedagogika, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_podroze, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_poezje_wiersze_liryka, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_polityka_politologia, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_popularnonaukowa, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_poradniki, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_powiesc, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_przyroda_rosliny_zwierzeta, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_psychologia_socjologia, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_publicystyka, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_religie, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_rozrywka_humor, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_sport, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_sztuka_malarstwo_architektura, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_wychowanie_rodzina, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_wywiady, URLS.baza_ksiazki)]
//        [InlineData(URLS.ksiazki_zdrowie_uroda, URLS.baza_ksiazki)]
//        //muzyka
//        [InlineData(URLS.muzyka_bestsellery, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_country_folk_gospel, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_dla_dzieci_i_mlodziezy, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_dvd_bluray, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_gadzety, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_hiphop_rap, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_jazz_blues, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_klasyczna, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_metal, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_non_ficition, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_nowosci, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_plyty_winylowe, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_pop, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_reggae_ska, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_rock_alternatywa, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_soundtrack_muzyka_filmowa, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_techno_disco_dance, URLS.baza_muzyka)]
//        [InlineData(URLS.muzyka_world_music, URLS.baza_muzyka)]
//        //oswietlenie
//        [InlineData(URLS.oswietlenie_akcesoria_oswietleniowe, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_czujniki_i_alarmy, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_lampy_przenosne, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_lamy_pozostale, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_lamy_stolowe_i_biurkowe, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_mierniki_i_przyrzady_pomiarowe, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_rozgalezniki_elektrycze, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_swietlowki_kompaktowe, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_tasmy_led, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_zarowki_led, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_zarowki_tradycyjne, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie_zerowki_halogenowe, URLS.baza_oswietlenie)]
//        [InlineData(URLS.oswietlenie__oswietlenie_dekoracyjne_i_nastrojowe, URLS.baza_oswietlenie)]
//        //rtv i telewizroy
//        [InlineData(URLS.rtv_i_telewizory_akcesoria_audio_video, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_bestsellery, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_car_audio_i_nawigacja, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_drobny_sprzet_audio_i_video, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_hifi_audio, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_kino_domowe, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_projektory, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_sluchawki_audio, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_telewizory, URLS.baza_rtv_i_telewizory)]
//        [InlineData(URLS.rtv_i_telewizory_tv_satelitarna_i_naziemna, URLS.baza_rtv_i_telewizory)]
//        //sport i rekreacja
//        [InlineData(URLS.sport_i_rekreacja_akcesoria, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_drony, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_elektryczne_deskorolki_i_hulajnogi, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_etui_opaski_na_ramie, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_kamery_sportowe, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_outdoor, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_rowery_elektryczne, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_sluchawki_sportowe, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_smartwache_i_opaski, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_urzadzenia_treningowe, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_zegarki_sportowe, URLS.baza_sport_i_rekreacja)]
//        [InlineData(URLS.sport_i_rekreacja_skating, URLS.baza_sport_i_rekreacja)]
//        //sprzet biurowy
//        [InlineData(URLS.sprzet_biurowy_bindownice, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_gilotyny_i_trymery, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_kalkulatory, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_kasoterminale, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_laminatory, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_meble_biurowe, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_niszczarki, URLS.baza_sprzet_biurowy)]
//        [InlineData(URLS.sprzet_biurowy_skanery, URLS.baza_sprzet_biurowy)]
//        //telefony i smartfony
//        [InlineData(URLS.telefony_i_smartfony_bestsellery, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_faksy, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_krotkofalowki, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_sluchawki_do_telefonu, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_smartfony, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_smartwatche, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_telefonia_internetowa, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_telefony_komorkowe, URLS.baza_telefony_i_smartfony)]
//        [InlineData(URLS.telefony_i_smartfony_telefony_stacjonarne, URLS.baza_telefony_i_smartfony)]
//        public void run(string url, string baza)
//        {
//            var go = new Mediamarkt_check_prices();
//            go.Check(url, baza);
//        }


//        //konsole i gry
//        [Theory]
//        [InlineData(URLS.m_konsole_i_gry_gry, URLS.baza_konsole_i_gry)]
//        //agd do zabudowy
//        [InlineData(URLS.m_agd_do_zabudowy_armatura_kuchenna, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.m_agd_do_zabudowy_lodowki_i_zamrazarki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.m_agd_do_zabudowy_okapy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.m_agd_do_zabudowy_piekarniki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.m_agd_do_zabudowy_plyty_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.m_agd_do_zabudowy_pralki_i_suszarki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        [InlineData(URLS.m_agd_do_zabudowy_zmywarki_do_zabudowy, URLS.baza_agd_do_zabudowy)]
//        //agd
//        [InlineData(URLS.m_agd_kuchenki_mikrofalowe, URLS.baza_agd)]
//        [InlineData(URLS.m_agd_kuchnie, URLS.baza_agd)]
//        [InlineData(URLS.m_agd_pralki_i_suszarki, URLS.baza_agd)]
//        [InlineData(URLS.m_agd_zmywarki, URLS.baza_agd)]
//        //agd male
//        [InlineData(URLS.m_agd_male_dom_czystosc_i_porzadek, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_ekspresy_do_kawy, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_kawa_i_inne_napoje, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_kuchnia_gotowanie_i_przygotowanie_potraw, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_oczyszczacze_i_nawlizacze_powietrza, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_odkurzacze, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_uroda_pielegnacja_i_zdrowie, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_male_uzdatnianie_powietrza, URLS.baza_agd_male)]
//        [InlineData(URLS.m_agd_pralki_i_suszarki, URLS.baza_agd_male)]
//        //filmy
//        [InlineData(URLS.m_filmy_blueray, URLS.baza_filmy)]
//        [InlineData(URLS.m_filmy_filmy_dvd, URLS.baza_filmy)]
//        //komputery i tablety
//        [InlineData(URLS.m_komputery_i_tabletymaterialy_eksploatacyjne_do_drukarek, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.m_komputery_i_tablety_akcesoria_komputerowe, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.m_komputery_i_tablety_czesci_komputerowe, URLS.baza_komputery_i_tablety)]
//        [InlineData(URLS.m_komputery_i_tablety_urzadzenia_sieciowe, URLS.baza_komputery_i_tablety)]
//        //sprzet biruowy
//        [InlineData(URLS.m_sprzet_biurowy_akcesoria_do_urzadzen_biurowych, URLS.baza_sprzet_biurowy)]
//        //telefony i smartfony
//        [InlineData(URLS.m_telefony_i_smartfony_akcesoria, URLS.baza_telefony_i_smartfony)]
//        //multiptle multiple
//        [InlineData(URLS.mm_agd_male_kawa_i_inne_napoje_herbaty, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_kawa_i_inne_napoje_kawy, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_kuchnia_gotowanie_artykuly_kuchenne, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_grzejniki, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_maszyny_do_szycia, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_oczyszczacze_parowe, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_odkurzacze, URLS.baza_agd_male)]
//        [InlineData(URLS.mm_agd_male_uzdatnianie_powietrza_wentylatory, URLS.baza_agd_male)]
//        public void multiple_run(string url, string baza)
//        {
//            var go = new Mediamarkt_check_prices();
//            go.Multiple_check(url, baza);
//        }

//    }
//}
