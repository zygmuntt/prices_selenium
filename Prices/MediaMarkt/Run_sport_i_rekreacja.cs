﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_sport_i_rekreacja
    {
        [Theory]
        [InlineData(URLS.sport_i_rekreacja_akcesoria, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_drony, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_elektryczne_deskorolki_i_hulajnogi, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_etui_opaski_na_ramie, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_kamery_sportowe, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_outdoor, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_rowery_elektryczne, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_sluchawki_sportowe, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_smartwache_i_opaski, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_urzadzenia_treningowe, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_zegarki_sportowe, URLS.baza_sport_i_rekreacja)]
        [InlineData(URLS.sport_i_rekreacja_skating, URLS.baza_sport_i_rekreacja)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
    }
}
