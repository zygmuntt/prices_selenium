﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_foto_i_kamery
    {
        [Theory]
        [InlineData(URLS.foto_i_kamery_akcesoria_fotograficzne, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_aparaty_cyfrowe, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_bestsellery, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_drony, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_drukarki_fotograficzne, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_kamery, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_lornetki_i_lunety, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_obiektywy_i_akcesoria, URLS.baza_foto_i_kamery)]
        [InlineData(URLS.foto_i_kamery_ramki_cyfrowe, URLS.baza_foto_i_kamery)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }

    }
}
