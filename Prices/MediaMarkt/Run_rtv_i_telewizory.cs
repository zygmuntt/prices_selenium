﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaMarkt
{
    public class Run_rtv_i_telewizory
    {
        [Theory]
        [InlineData(URLS.rtv_i_telewizory_akcesoria_audio_video, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_bestsellery, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_car_audio_i_nawigacja, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_drobny_sprzet_audio_i_video, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_hifi_audio, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_kino_domowe, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_projektory, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_sluchawki_audio, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_telewizory, URLS.baza_rtv_i_telewizory)]
        [InlineData(URLS.rtv_i_telewizory_tv_satelitarna_i_naziemna, URLS.baza_rtv_i_telewizory)]
        public void run(string url, string baza)
        {
            var go = new Mediamarkt_check_prices();
            go.Check(url, baza);
        }
    }
}
