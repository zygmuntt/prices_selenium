﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
    public class Run_agd_male
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_agd_male_dla_dziecka)]
        [InlineData(MediaExpert.URLS.m_agd_male_do_domu)]
        [InlineData(MediaExpert.URLS.m_agd_male_do_kuchni)]
        [InlineData(MediaExpert.URLS.m_agd_male_higiena_i_uroda)]
        [InlineData(MediaExpert.URLS.m_agd_male_akcesoria_kuchenne)]

        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }
    }
}
