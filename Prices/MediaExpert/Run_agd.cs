﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
  public  class Run_agd
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_agd_akcesoria_agd)]
        [InlineData(MediaExpert.URLS.m_agd_kuchnie_wolnostojace)]
        [InlineData(MediaExpert.URLS.m_agd_lodowki_i_zamrazarki)]
        [InlineData(MediaExpert.URLS.m_agd_pralki_i_suszarki)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }

        [Theory]
        [InlineData(MediaExpert.URLS.s_agd_okapy)]
        [InlineData(MediaExpert.URLS.s_agd_zmywarki)]
        public void Run_single(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }
    }
}
