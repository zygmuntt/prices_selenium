﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
   public class Run_telewizory_i_rtv
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_akcesoria_rtv)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_blue_ray_dvd_nagrywarki)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_car_audio)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_drobny_sprzet_rtv)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_hifi_audio)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_kina_domowe)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_nawigacje_gps)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_party)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_projektory)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_sluchawki)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_telewizda_dvb_i_satelitarna)]
        [InlineData(MediaExpert.URLS.m_telewizory_i_rtv_uchwyty_do_tv)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }


        [Theory]
        [InlineData(MediaExpert.URLS.s_telewizory_i_rtv_ipod_odtwarzacze_mp3_mp4)]
        [InlineData(MediaExpert.URLS.s_telewizory_i_rtv_power_audio)]
        [InlineData(MediaExpert.URLS.s_telewizory_i_rtv_telwizory)]
        public void Run_single(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }


    }
}
