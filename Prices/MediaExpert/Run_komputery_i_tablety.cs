﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
public    class Run_komputery_i_tablety
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_akcesoria_komputerowe)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_drukarki_i_tusze)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_elementy_sieciowe)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_komputery_pc)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_laptopy_i_ultrabooki)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_oprogramowanie)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_peryferia_komputerowe)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_podzespoly_komputerowe)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_routery)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_tablety_i_ebooki)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_tusze_i_tonery)]
        [InlineData(MediaExpert.URLS.m_komputery_i_tablety_urzadzenia_biurowe)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }


        [Theory]
        [InlineData(MediaExpert.URLS.s_komputery_i_tablety_monitory_led)]
        public void Run_single(string url)
        {
            var go =new Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }

    }
}
