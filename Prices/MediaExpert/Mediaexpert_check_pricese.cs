﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Xunit;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Prices.MediaExpert
{
    public class Mediaexpert_check_pricese
    {

        private IWebDriver driver;
        public void check_prices_single(string url)
        {

            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            string pages;
            string pages_count;
            int z = 0;
            int k = 0;
            int pages_int = 0;
            try
            {
                pages = driver.FindElement(By.CssSelector("div.list_nav:nth-child(4) > div:nth-child(1) > span:nth-child(2)")).Text;
                pages_count = pages;
                pages_count = pages_count.Replace("z ", "");
                pages_int = Convert.ToInt32(pages_count);
            }
            catch { }
            var cookies = driver.FindElement(By.CssSelector("#cookieBoxContainer > div > div.m-btn.m-btn-primary.m-cookieBox_closeBtn.js-cookieBox"));
            cookies.Click();
            int if_pages_int = pages_int;
            var dropdown = driver.FindElement(By.CssSelector("#formPorownanie > div.list_head.b-offer_filters.clearfix2 > div:nth-child(2) > div.view_on_desktop > div.list_sort > div > em"));
            dropdown.Click(); //cannot do select element, dropdown is not select.
            var asc = driver.FindElement(By.CssSelector("#formPorownanie > div.list_head.b-offer_filters.clearfix2 > div:nth-child(2) > div.view_on_desktop > div.list_sort > div > ul > li:nth-child(2)"));
            asc.Click();
            do
            {
                try
                {
                    var close_newsletter = driver.FindElement(By.CssSelector("body > div.mfp-wrap.mfp-close-btn-in.mfp-auto-cursor.mfp-ready > div > div.mfp-content > div > button"));
                    close_newsletter.Click();
                }
                catch { }

                IList<IWebElement> article_names = driver.FindElements(By.XPath("//h2[@class='m-product_title is-desktop' and @data-atat='name']//a[@class='js-gtmEvent_click' and @data-gtm-event='productClick' and @data-gtm-data-eventlabel='nazwa produktu']"));
                IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='m-product_options is-desktop']//p[@class='price price_txt' and @data-atat='price']"));
                IList<IWebElement> local_id = driver.FindElements(By.XPath("//article[contains(@id, 'product-id')]"));
                IList<IWebElement> no_prices = driver.FindElements(By.XPath("//div[@class='b-productInaccessible js-buy_info_sticky' or @class='b-productInaccessibleZero js-buy_info_sticky']//p[@class='m-typo m-typo_text is-small' or @class='m-typo m-typo_text']"));
                for (int i = 0; i < article_names.Count(); i++)
                {
                    var name = article_names[i].Text;
                    var id = local_id[i].GetAttribute("id").Replace("product-id-", "");
                   name =  name.Replace("'", "");
                    if (i >= prices.Count)
                    {
                        var price_correct = no_prices[z].Text;
                        z++;
                        AGAIN:
                        try
                        {
                            SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, Debug, MediaExpert_Local_id) values('{name}', '{price_correct}', '{id}') else update prices.dbo.Articles set Debug = '{price_correct}', MediaExpert_Local_id = '{id}' where name = '{name}'");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN;
                        }
                        AGAIN2:
                        try
                        {
                            SQL.Insert($"begin if not exists (select* from [prices].[dbo].[MediaExpert] where no_price = '{price_correct}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[MediaExpert] (local_id,no_price,date,name) values ('{id}','{price_correct}','{time}','{name}') end end ");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN2;
                        }

                    }
                    else
                    {
                        var price = prices[i].Text;
                        var price_temp = price.Substring(Math.Max(0, price.Length - 2));
                        var price_correct = price.Remove(price.Length - 2) + "." + price_temp;
                       
                        AGAIN:
                        try
                        {
                            SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, Price_MediaExpert, MediaExpert_Local_id) values('{name}', '{price_correct}', '{id}') else update prices.dbo.Articles set Price_MediaExpert = '{price_correct}', MediaExpert_Local_id = '{id}' where name = '{name}'");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN;
                        }
                        AGAIN2:
                        try
                        {
                            SQL.Insert($"begin if not exists (select* from [prices].[dbo].[MediaExpert] where price = '{price_correct}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[MediaExpert] (local_id,price,date,name) values ('{id}','{price_correct}','{time}','{name}') end end ");
                        }
                        catch (System.Data.SqlClient.SqlException deadlock)
                        {
                            if (deadlock.Number == 1205)
                                goto AGAIN2;
                        }
                    }


                }
                if (if_pages_int > 1)
                {
                    var next_page_click = driver.FindElement(By.XPath("//a[@class='js-listNav_link js-gtmEvent_click js-gtmNoRedirect js-gtmData' and @data-gtm-data-eventaction='Widok: next']//span[@class='icon-arrow-right']"));
                    next_page_click.Click();
                    if_pages_int--;
                }
                z = 0;
                k++;
            }
            while (k < pages_int);
            driver.Quit();
        }
    

        public void check_prices_multiple(string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = url;
            string time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            System.Threading.Thread.Sleep(1000);
            IList<IWebElement> categories = driver.FindElements(By.ClassName("cat_box_content"));
            string pages;
            string pages_count;
            var cookies = driver.FindElement(By.CssSelector("#cookieBoxContainer > div > div.m-btn.m-btn-primary.m-cookieBox_closeBtn.js-cookieBox"));
            cookies.Click();
            for (int j = 0; j < categories.Count; j++)
            {
                int z = 0;
                int k = 0;
                int pages_int = 0;
                categories = driver.FindElements(By.ClassName("cat_box_content"));
                categories[j].Click();
                try
                {
                    pages = driver.FindElement(By.CssSelector("div.list_nav:nth-child(4) > div:nth-child(1) > span:nth-child(2)")).Text;
                    pages_count = pages;
                    pages_count = pages_count.Replace("z ", "");
                    pages_int = Convert.ToInt32(pages_count);
                }
                catch { }
                int if_pages_int = pages_int;
                var dropdown = driver.FindElement(By.CssSelector("#formPorownanie > div.list_head.b-offer_filters.clearfix2 > div:nth-child(2) > div.view_on_desktop > div.list_sort > div > em"));
                dropdown.Click(); //cannot do select element, dropdown is not select.
                var asc = driver.FindElement(By.CssSelector("#formPorownanie > div.list_head.b-offer_filters.clearfix2 > div:nth-child(2) > div.view_on_desktop > div.list_sort > div > ul > li:nth-child(2)"));
                asc.Click();
                do
                {
                    try
                    {
                        var close_newsletter = driver.FindElement(By.CssSelector("body > div.mfp-wrap.mfp-close-btn-in.mfp-auto-cursor.mfp-ready > div > div.mfp-content > div > button"));
                        close_newsletter.Click();
                    }
                    catch { }

                    IList<IWebElement> article_names = driver.FindElements(By.XPath("//h2[@class='m-product_title is-desktop' and @data-atat='name']//a[@class='js-gtmEvent_click' and @data-gtm-event='productClick' and @data-gtm-data-eventlabel='nazwa produktu']"));
                    IList<IWebElement> prices = driver.FindElements(By.XPath("//div[@class='m-product_options is-desktop']//p[@class='price price_txt' and @data-atat='price']"));
                    IList<IWebElement> local_id = driver.FindElements(By.XPath("//article[contains(@id, 'product-id')]"));
                    IList<IWebElement> no_prices = driver.FindElements(By.XPath("//div[@class='b-productInaccessible js-buy_info_sticky' or @class='b-productInaccessibleZero js-buy_info_sticky']//p[@class='m-typo m-typo_text is-small' or @class='m-typo m-typo_text']"));
                    for (int i = 0; i < article_names.Count(); i++)
                    {
                        var name = article_names[i].Text;
                        var id = local_id[i].GetAttribute("id").Replace("product-id-", "");
                       name = name.Replace("'", "");
                        if (i>=prices.Count)
                        {
                           var price_correct = no_prices[z].Text;
                            z++;
                            AGAIN:
                            try
                            {
                                SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, debug, MediaExpert_Local_id) values('{name}', '{price_correct}', '{id}') else update prices.dbo.Articles set debug = '{price_correct}', MediaExpert_Local_id = '{id}' where name = '{name}'");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN;
                            }
                            AGAIN2:
                            try
                            {
                                SQL.Insert($"begin if not exists (select* from [prices].[dbo].[MediaExpert] where no_price = '{price_correct}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[MediaExpert] (local_id,no_price,date,name) values ('{id}','{price_correct}','{time}','{name}') end end ");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN2;
                            }
                        }
                        else
                        {
                            var  price = prices[i].Text;
                            var price_temp = price.Substring(Math.Max(0, price.Length - 2));
                            var price_correct = price.Remove(price.Length - 2) + "." + price_temp;
                            AGAIN:
                            try
                            {
                                SQL.Insert($"if not exists (select* from prices.dbo.Articles where Name like '{name}') insert into prices.dbo.Articles(name, Price_MediaExpert, MediaExpert_Local_id) values('{name}', '{price_correct}', '{id}') else update prices.dbo.Articles set Price_MediaExpert = '{price_correct}', MediaExpert_Local_id = '{id}' where name = '{name}'");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                goto AGAIN;
                            }
                            AGAIN2:
                            try
                            {
                                SQL.Insert($"begin if not exists (select* from [prices].[dbo].[MediaExpert] where price = '{price_correct}' and name = '{name}' and date='{time}' and local_id = '{id}') begin Insert into [prices].[dbo].[MediaExpert] (local_id,price,date,name) values ('{id}','{price_correct}','{time}','{name}') end end ");
                            }
                            catch (System.Data.SqlClient.SqlException deadlock)
                            {
                                if (deadlock.Number == 1205)
                                    goto AGAIN2;
                            }
                        }
                        

                    }
                    if (if_pages_int > 1)
                    {
                        var next_page_click = driver.FindElement(By.XPath("//a[@class='js-listNav_link js-gtmEvent_click js-gtmNoRedirect js-gtmData' and @data-gtm-data-eventaction='Widok: next']//span[@class='icon-arrow-right']"));
                        next_page_click.Click();
                        if_pages_int--;
                    }
                    z = 0;
                    k++;
                }
                while (k < pages_int);

                driver.Url = url;
            }
            driver.Quit();
        }

    }
}
