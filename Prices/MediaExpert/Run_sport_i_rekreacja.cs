﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
  public  class Run_sport_i_rekreacja
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_akcesoria_do_pojazdow)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_akcesoria_do_rowerow)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_badminton)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_czesci_rowerowe)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_dart)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_fitness_i_silownia)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_koszykowka)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_pilka_nozna)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_pozostale_sporty)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_rowery)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_siatkowka)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_skating)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_sporty_walki)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_sporty_wodne)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_sporty_zimowe)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_sprzet_pomiarowy)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_stoly_do_gry)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_tenis_stolowy)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_tenis_ziemny_i_squash)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_turystyka)]
        [InlineData(MediaExpert.URLS.m_sport_i_rekreacja_unihokej)]
        public void Run_multiple(string url)
        {
            var go = new MediaExpert.Mediaexpert_check_pricese();
            go.check_prices_multiple(url);

        }

        [Theory]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_deskorolki_elektryczne)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_hulajnogi)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_hulajnogi_elektryczne)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_kamery_sportowe)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_motorowery)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_obuwie)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_odziez)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_quady)]
        [InlineData(MediaExpert.URLS.s_sport_i_rekreacja_skutery)]
        public void Run_single(string url)
        {
            var go = new MediaExpert.Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }

    }
}
