﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
    public class Run_konsole_i_gry
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_nintendo)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_pc)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_playstation3)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_playstation4)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_pozostale_platformy)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_xbox_360)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_xbox_one)]
        [InlineData(MediaExpert.URLS.m_konsole_i_gry_Zamow_przed_premiera)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);

        }

        [Theory]
        [InlineData(MediaExpert.URLS.s_konsole_i_gry_akcesoria_do_gier)]
        [InlineData(MediaExpert.URLS.s_konsole_i_gry_biurka_gamingowe)]
        [InlineData(MediaExpert.URLS.s_konsole_i_gry_fotele_gamingowe)]
        [InlineData(MediaExpert.URLS.s_konsole_i_gry_gry_planszowe)]
        [InlineData(MediaExpert.URLS.s_konsole_i_gry_kontrolery_pady_kierownice)]
        [InlineData(MediaExpert.URLS.s_konsole_i_gry_okulary_vr)]
        public void Run_single(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }
    }
}
