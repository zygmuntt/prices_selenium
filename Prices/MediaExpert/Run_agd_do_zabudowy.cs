﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
   public class Run_agd_do_zabudowy
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_agd_do_zabudowy_akcesoria_do_zabudowy)]
        [InlineData(MediaExpert.URLS.m_agd_do_zabudowy_lodowki_i_zamrazarki_do_zabudowy)]
        [InlineData(MediaExpert.URLS.m_agd_do_zabudowy_pralki_i_suszarki_do_zabudowy)]
        [InlineData(MediaExpert.URLS.m_agd_do_zabudowy_szuflady_do_podgrzewania_naczyn)]
        [InlineData(MediaExpert.URLS.m_agd_do_zabudowy_zlewozmywaki_i_baterie_kuchenne)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }

        [Theory]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_ekspresy_do_zabudowy)]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_kuchenki_mikrofalowe_do_zabudowy)]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_kuchnie_do_zabudowy)]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_okapy_do_zabudowy)]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_piekarniki_do_zabudowy)]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_plyty_do_zabudowy)]
        [InlineData(MediaExpert.URLS.s_agd_do_zabudowy_zmywarki_do_zabudowy)]
        public void Run_single (string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }
    }
}
