﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using OpenQA.Selenium.Chrome;

namespace Prices.MediaExpert
{
    public class Run_dom_i_ogrod
    {

        [Theory]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_architektura_ogrodowa)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_artykuly_dla_zwierzat)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_baseny_i_akcesoria)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_dekoracje_ogrodowe)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_do_domu)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_elektronarzedzia)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_grille)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_inteligentny_dom)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_kosiarki_i_podkaszarki)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_lazienka)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_maszyny_ogrodowe)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_meble_ogrodowe)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_myjki_wysokocisnieniowe_i_akcesoria)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_narzedzia_akumulatorowe)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_narzedzia_ogrodnicze)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_narzedzia_reczne)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_nawadnianie)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_oswietlenie)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_oswietlenie_ogrodowe)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_porzadki_w_ogrodzie)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_relaks_w_ogrodzie)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_utrzymanie_czystosci)]
        [InlineData(MediaExpert.URLS.m_dom_i_ogrod_wyposazenie_warsztatu)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }







    }
}

