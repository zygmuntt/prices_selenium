﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using OpenQA.Selenium.Chrome;
namespace Prices.MediaExpert
{
    public class Run_foto_i_kamery
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_foto_i_kamery_akcesoria_do_aparatow_i_kamer)]
        [InlineData(MediaExpert.URLS.m_foto_i_kamery_drony_i_akcesoria)]
        [InlineData(MediaExpert.URLS.m_foto_i_kamery_kamery_sportowe_i_akcesoria)]
        [InlineData(MediaExpert.URLS.m_foto_i_kamery_optyka)]
        [InlineData(MediaExpert.URLS.m_foto_i_kamery_ramki_foto)]
        public void Run_multiple(string url)
        {
            var go = new MediaExpert.Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }

        [Theory]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_aparaty_kompaktowe)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_aparaty_natychmiastowe)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_aparaty_z_wymienna_optyka)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_drukarki_do_zdjec)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_gimbale)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_kamery_360)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_kamery_cyfrowe)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_kamery_ip)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_lustrzanki_cyfrowe)]
        [InlineData(MediaExpert.URLS.s_foto_i_kamery_wideorejestratory)]
        public void Run_single(string url)
        {
            var go = new MediaExpert.Mediaexpert_check_pricese();
            go.check_prices_single(url);

        }


    }
}
