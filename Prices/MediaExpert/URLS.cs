﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prices.MediaExpert
{
   public class URLS
    {


        //telewizory i rtv
        public const string s_telewizory_i_rtv_telwizory = "https://www.mediaexpert.pl/telewizory/";
        public const string m_telewizory_i_rtv_blue_ray_dvd_nagrywarki = "https://www.mediaexpert.pl/blu-ray-dvd-nagrywarki/";
        public const string m_telewizory_i_rtv_kina_domowe = "https://www.mediaexpert.pl/kina-domowe/";
        public const string m_telewizory_i_rtv_hifi_audio = "https://www.mediaexpert.pl/hi-fi-audio/";
        public const string m_telewizory_i_rtv_drobny_sprzet_rtv= "https://www.mediaexpert.pl/drobny-sprzet-rtv/";
        public const string m_telewizory_i_rtv_sluchawki = "https://www.mediaexpert.pl/sluchawki/";
        public const string s_telewizory_i_rtv_ipod_odtwarzacze_mp3_mp4 = "https://www.mediaexpert.pl/ipod-odtwarzacze-mp3-mp4/";
        public const string m_telewizory_i_rtv_car_audio = "https://www.mediaexpert.pl/car-audio/";
        public const string m_telewizory_i_rtv_nawigacje_gps = "https://www.mediaexpert.pl/nawigacje-gps/";
        public const string m_telewizory_i_rtv_telewizda_dvb_i_satelitarna = "https://www.mediaexpert.pl/telewizja-dvb-t-i-satelitarna/";
        public const string m_telewizory_i_rtv_uchwyty_do_tv = "https://www.mediaexpert.pl/uchwyty-do-tv/";
        public const string m_telewizory_i_rtv_projektory = "https://www.mediaexpert.pl/projektory/";
        public const string m_telewizory_i_rtv_party = "https://www.mediaexpert.pl/party/";
        public const string s_telewizory_i_rtv_power_audio = "https://www.mediaexpert.pl/power-audio/";
        public const string m_telewizory_i_rtv_akcesoria_rtv = "https://www.mediaexpert.pl/akcesoria-rtv/";

        //agd
        public const string m_agd_lodowki_i_zamrazarki = "https://www.mediaexpert.pl/lodowki-i-zamrazarki/";
        public const string m_agd_pralki_i_suszarki= "https://www.mediaexpert.pl/pralki-i-suszarki/";
        public const string m_agd_kuchnie_wolnostojace = "https://www.mediaexpert.pl/kuchnie/";
        public const string s_agd_zmywarki = "https://www.mediaexpert.pl/zmywarki/";
        public const string s_agd_okapy = "https://www.mediaexpert.pl/okapy-do-zabudowy/";
        public const string m_agd_akcesoria_agd = "https://www.mediaexpert.pl/akcesoria-agd/";

        //agd do zabudowy
        public const string s_agd_do_zabudowy_piekarniki_do_zabudowy = "https://www.mediaexpert.pl/piekarniki-do-zabudowy/";
        public const string s_agd_do_zabudowy_plyty_do_zabudowy = "https://www.mediaexpert.pl/plyty-do-zabudowy/";
        public const string s_agd_do_zabudowy_okapy_do_zabudowy = "https://www.mediaexpert.pl/okapy-do-zabudowy/";
        public const string s_agd_do_zabudowy_zmywarki_do_zabudowy = "https://www.mediaexpert.pl/zmywarki-do-zabudowy/";
        public const string s_agd_do_zabudowy_kuchenki_mikrofalowe_do_zabudowy = "https://www.mediaexpert.pl/kuchnie-mikrofalowe-do-zabudowy/";
        public const string m_agd_do_zabudowy_lodowki_i_zamrazarki_do_zabudowy = "https://www.mediaexpert.pl/lodowki-i-zamrazarki-do-zabudowy/";
        public const string s_agd_do_zabudowy_kuchnie_do_zabudowy = "https://www.mediaexpert.pl/kuchnie-zestawy-do-zabudowy/";
        public const string s_agd_do_zabudowy_ekspresy_do_zabudowy = "https://www.mediaexpert.pl/ekspresy-do-zabudowy/";
        public const string m_agd_do_zabudowy_pralki_i_suszarki_do_zabudowy = "https://www.mediaexpert.pl/pralki-i-suszarki-do-zabudowy/";
        public const string m_agd_do_zabudowy_szuflady_do_podgrzewania_naczyn = "https://www.mediaexpert.pl/szuflady-do-podgrzewania-naczyn/";
        public const string m_agd_do_zabudowy_zlewozmywaki_i_baterie_kuchenne = "https://www.mediaexpert.pl/zlewozmywaki-i-baterie-kuchenne/";
        public const string m_agd_do_zabudowy_akcesoria_do_zabudowy = "https://www.mediaexpert.pl/akcesoria-do-zabudowy/";

        //agd male
        public const string m_agd_male_do_domu = "https://www.mediaexpert.pl/agd-male-do-domu/";
        public const string m_agd_male_higiena_i_uroda = "https://www.mediaexpert.pl/higiena-i-uroda/";
        public const string m_agd_male_do_kuchni= "https://www.mediaexpert.pl/do-kuchni/";
        public const string m_agd_male_akcesoria_kuchenne = "https://www.mediaexpert.pl/akcesoria-agd-drobne/";
        public const string m_agd_male_dla_dziecka = "https://www.mediaexpert.pl/dla-dziecka/";

        //komputery i tablety
        public const string m_komputery_i_tablety_tablety_i_ebooki = "https://www.mediaexpert.pl/tablety-i-e-booki/";
        public const string m_komputery_i_tablety_komputery_pc = "https://www.mediaexpert.pl/komputery-pc/";
        public const string s_komputery_i_tablety_monitory_led = "https://www.mediaexpert.pl/monitory-led/";
        public const string m_komputery_i_tablety_podzespoly_komputerowe = "https://www.mediaexpert.pl/podzespoly-komputerowe/";
        public const string m_komputery_i_tablety_oprogramowanie = "https://www.mediaexpert.pl/oprogramowanie/";
        public const string m_komputery_i_tablety_drukarki_i_tusze = "https://www.mediaexpert.pl/drukarki-i-tusze/";
        public const string m_komputery_i_tablety_urzadzenia_biurowe = "https://www.mediaexpert.pl/urzadzenia-biurowe/";
        public const string m_komputery_i_tablety_tusze_i_tonery = "https://www.mediaexpert.pl/tusze-i-tonery/";
        public const string m_komputery_i_tablety_routery = "https://www.mediaexpert.pl/routery/";
        public const string m_komputery_i_tablety_elementy_sieciowe = "https://www.mediaexpert.pl/elementy-sieciowe/";
        public const string m_komputery_i_tablety_peryferia_komputerowe = "https://www.mediaexpert.pl/peryferia-komputerowe/";
        public const string m_komputery_i_tablety_akcesoria_komputerowe = "https://www.mediaexpert.pl/akcesoria-komputerowe/";
        public const string m_komputery_i_tablety_laptopy_i_ultrabooki = "https://www.mediaexpert.pl/laptopy-i-ultrabooki/";

        //smartfony i telefony
        public const string s_smartfony_i_telefony_smartfony = "https://www.mediaexpert.pl/smartfony/";
        public const string s_smartfony_i_telefony_smartwatche_i_zegarki = "https://www.mediaexpert.pl/smartwatche-i-zegarki/";
        public const string s_smartfony_i_telefony_okulary_vr = "https://www.mediaexpert.pl/okulary-vr/";
        public const string s_smartfony_i_telefony_telefony_komorkowe = "https://www.mediaexpert.pl/telefony-komorkowe/";
        public const string m_smartfony_i_telefony_telefony_stacjonarne_i_faksy = "https://www.mediaexpert.pl/telefony-stacjonarne-i-faksy/";
        public const string m_smartfony_i_telefony_nawigacje_gps = "https://www.mediaexpert.pl/nawigacje-gps/";
        public const string m_smartfony_i_telefony_cb_radio_telefony = "https://www.mediaexpert.pl/cb-i-radiotelefony/";
        public const string m_smartfony_i_telefony_akcesoria_do_telefonow = "https://www.mediaexpert.pl/akcesoria-do-telefonow/";
        public const string s_smartfony_i_telefony_gimbale = "https://www.mediaexpert.pl/gimbale/";

        //foto i kamery
        public const string m_foto_i_kamery_kamery_sportowe_i_akcesoria = "https://www.mediaexpert.pl/kamery-sportowe-i-akcesoria/";
        public const string s_foto_i_kamery_kamery_360 = "https://www.mediaexpert.pl/kamery-360/";
        public const string s_foto_i_kamery_kamery_ip = "https://www.mediaexpert.pl/kamery-ip/";
        public const string s_foto_i_kamery_kamery_cyfrowe = "https://www.mediaexpert.pl/kamery-cyfrowe/";
        public const string s_foto_i_kamery_lustrzanki_cyfrowe = "https://www.mediaexpert.pl/lustrzanki-cyfrowe/";
        public const string s_foto_i_kamery_aparaty_kompaktowe = "https://www.mediaexpert.pl/aparaty-kompaktowe/";
        public const string s_foto_i_kamery_aparaty_z_wymienna_optyka = "https://www.mediaexpert.pl/aparaty-z-wymienna-optyka-bezlustrowce-/";
        public const string s_foto_i_kamery_aparaty_natychmiastowe = "https://www.mediaexpert.pl/Aparaty-natychmiastowe/";
        public const string s_foto_i_kamery_drukarki_do_zdjec = "https://www.mediaexpert.pl/Drukarki-do-zdjec/";
        public const string m_foto_i_kamery_akcesoria_do_aparatow_i_kamer = "https://www.mediaexpert.pl/akcesoria-do-aparatow-i-kamer/";
        public const string m_foto_i_kamery_ramki_foto = "https://www.mediaexpert.pl/ramki-foto/";
        public const string s_foto_i_kamery_wideorejestratory = "https://www.mediaexpert.pl/wideorejestratory/";
        public const string m_foto_i_kamery_drony_i_akcesoria = "https://www.mediaexpert.pl/drony-i-akcesoria/";
        public const string m_foto_i_kamery_optyka = "https://www.mediaexpert.pl/optyka/";
        public const string s_foto_i_kamery_gimbale = "https://www.mediaexpert.pl/gimbale/";


        //konsole i gry
        public const string m_konsole_i_gry_playstation4 = "https://www.mediaexpert.pl/playstation-4/";
        public const string m_konsole_i_gry_playstation3 = "https://www.mediaexpert.pl/playstation-3/";
        public const string m_konsole_i_gry_xbox_one = "https://www.mediaexpert.pl/xbox-one/";
        public const string m_konsole_i_gry_xbox_360 = "https://www.mediaexpert.pl/xbox360/";
        public const string m_konsole_i_gry_pc= "https://www.mediaexpert.pl/pc/";
        public const string m_konsole_i_gry_nintendo = "https://www.mediaexpert.pl/konsole-gry-nintendo/";
        public const string m_konsole_i_gry_pozostale_platformy = "https://www.mediaexpert.pl/pozostale-platformy/";
        public const string m_konsole_i_gry_Zamow_przed_premiera= "https://www.mediaexpert.pl/zamow-przed-premiera/";
        public const string s_konsole_i_gry_biurka_gamingowe = "https://www.mediaexpert.pl/biurka-gamingowe/";
        public const string s_konsole_i_gry_fotele_gamingowe = "https://www.mediaexpert.pl/fotele-gamingowe/";
        public const string s_konsole_i_gry_kontrolery_pady_kierownice = "https://www.mediaexpert.pl/kontrolery-pady-kierownice/";
        public const string s_konsole_i_gry_okulary_vr = "https://www.mediaexpert.pl/okulary-vr/";
        public const string s_konsole_i_gry_gry_planszowe = "https://www.mediaexpert.pl/gry-planszowe-dla-dzieci/";
        public const string s_konsole_i_gry_akcesoria_do_gier = "https://www.mediaexpert.pl/akcesoria-do-gier/";

        //sport i rekreacja
        public const string m_sport_i_rekreacja_rowery = "https://www.mediaexpert.pl/rowery/";
        public const string s_sport_i_rekreacja_hulajnogi = "https://www.mediaexpert.pl/hulajnogi/";
        public const string s_sport_i_rekreacja_hulajnogi_elektryczne = "https://www.mediaexpert.pl/hulajnogi-elektryczne/";
        public const string s_sport_i_rekreacja_deskorolki_elektryczne = "https://www.mediaexpert.pl/deskorolki-elektryczne/";
        public const string m_sport_i_rekreacja_czesci_rowerowe = "https://www.mediaexpert.pl/czesci-rowerowe/";
        public const string m_sport_i_rekreacja_akcesoria_do_rowerow = "https://www.mediaexpert.pl/akcesoria-do-rowerow/";
        public const string s_sport_i_rekreacja_motorowery = "https://www.mediaexpert.pl/motorowery/";
        public const string s_sport_i_rekreacja_skutery = "https://www.mediaexpert.pl/skutery/";
        public const string s_sport_i_rekreacja_quady = "https://www.mediaexpert.pl/quady/";
        public const string m_sport_i_rekreacja_akcesoria_do_pojazdow = "https://www.mediaexpert.pl/akcesoria-do-pojazdow/";
        public const string s_sport_i_rekreacja_kamery_sportowe = "https://www.mediaexpert.pl/kamery-sportowe/";
        public const string m_sport_i_rekreacja_fitness_i_silownia = "https://www.mediaexpert.pl/fitness-i-silownia/";
        public const string m_sport_i_rekreacja_sprzet_pomiarowy = "https://www.mediaexpert.pl/sprzet-pomiarowy/";
        public const string m_sport_i_rekreacja_skating = "https://www.mediaexpert.pl/skating/";
        public const string m_sport_i_rekreacja_koszykowka = "https://www.mediaexpert.pl/koszykowka/";
        public const string m_sport_i_rekreacja_pilka_nozna = "https://www.mediaexpert.pl/pilka-nozna/";
        public const string m_sport_i_rekreacja_siatkowka = "https://www.mediaexpert.pl/siatkowka/";
        public const string m_sport_i_rekreacja_unihokej = "https://www.mediaexpert.pl/unihokej/";
        public const string m_sport_i_rekreacja_tenis_stolowy = "https://www.mediaexpert.pl/tenis-stolowy/";
        public const string m_sport_i_rekreacja_tenis_ziemny_i_squash = "https://www.mediaexpert.pl/tenis-ziemny-i-squash/";
        public const string m_sport_i_rekreacja_badminton = "https://www.mediaexpert.pl/badminton/";
        public const string m_sport_i_rekreacja_dart = "https://www.mediaexpert.pl/dart/";
        public const string m_sport_i_rekreacja_sporty_walki = "https://www.mediaexpert.pl/sporty-walki/";
        public const string m_sport_i_rekreacja_sporty_wodne = "https://www.mediaexpert.pl/sporty-wodne/";
        public const string m_sport_i_rekreacja_turystyka = "https://www.mediaexpert.pl/turystyka/";
        public const string m_sport_i_rekreacja_stoly_do_gry = "https://www.mediaexpert.pl/pilkarzyki/";
        public const string m_sport_i_rekreacja_sporty_zimowe = "https://www.mediaexpert.pl/sporty-zimowe/";
        public const string m_sport_i_rekreacja_pozostale_sporty = "https://www.mediaexpert.pl/pozostale-sporty/";
        public const string s_sport_i_rekreacja_odziez = "https://www.mediaexpert.pl/odziez/";
        public const string s_sport_i_rekreacja_obuwie = "https://www.mediaexpert.pl/obuwie/";

        // dom i ogord
        public const string m_dom_i_ogrod_kosiarki_i_podkaszarki = "https://www.mediaexpert.pl/kosiarki-i-podkaszarki/";
        public const string m_dom_i_ogrod_maszyny_ogrodowe= "https://www.mediaexpert.pl/maszyny-ogrodowe/";
        public const string m_dom_i_ogrod_myjki_wysokocisnieniowe_i_akcesoria = "https://www.mediaexpert.pl/myjki-wysokocisnieniowe_i_akcesoria/";
        public const string m_dom_i_ogrod_narzedzia_akumulatorowe = "https://www.mediaexpert.pl/narzedzia-akumulatorowe/";
        public const string m_dom_i_ogrod_narzedzia_ogrodnicze = "https://www.mediaexpert.pl/narzedzia-ogrodowe/";
        public const string m_dom_i_ogrod_porzadki_w_ogrodzie = "https://www.mediaexpert.pl/porzadki-w-ogrodzie/";
        public const string m_dom_i_ogrod_grille = "https://www.mediaexpert.pl/grille/";
        public const string m_dom_i_ogrod_architektura_ogrodowa = "https://www.mediaexpert.pl/architektura-ogrodowa/";
        public const string m_dom_i_ogrod_relaks_w_ogrodzie = "https://www.mediaexpert.pl/relaks-w-ogrodzie/";
        public const string m_dom_i_ogrod_meble_ogrodowe = "https://www.mediaexpert.pl/meble-ogrodowe/";
        public const string m_dom_i_ogrod_oswietlenie_ogrodowe = "https://www.mediaexpert.pl/oswietlenie-ogrodowe/";
        public const string m_dom_i_ogrod_baseny_i_akcesoria = "https://www.mediaexpert.pl/baseny-i-akcesoria/";
        public const string m_dom_i_ogrod_do_domu = "https://www.mediaexpert.pl/do-domu/";
        public const string m_dom_i_ogrod_inteligentny_dom = "https://www.mediaexpert.pl/inteligentny-dom/";
        public const string m_dom_i_ogrod_nawadnianie = "https://www.mediaexpert.pl/nawadnianie/";
        public const string m_dom_i_ogrod_dekoracje_ogrodowe = "https://www.mediaexpert.pl/dekoracje-ogrodowe/";
        public const string m_dom_i_ogrod_lazienka = "https://www.mediaexpert.pl/lazienka/";
        public const string m_dom_i_ogrod_oswietlenie = "https://www.mediaexpert.pl/oswietlenie/";
        public const string m_dom_i_ogrod_utrzymanie_czystosci = "https://www.mediaexpert.pl/utrzymanie-czystosci/";
        public const string m_dom_i_ogrod_elektronarzedzia = "https://www.mediaexpert.pl/elektronarzedzia-i-akcesoria/";
        public const string m_dom_i_ogrod_narzedzia_reczne = "https://www.mediaexpert.pl/narzedzia-reczne/";
        public const string m_dom_i_ogrod_wyposazenie_warsztatu = "https://www.mediaexpert.pl/wyposazenie-warsztatu/";
        public const string m_dom_i_ogrod_artykuly_dla_zwierzat = "https://www.mediaexpert.pl/artykuly-dla-zwierzat/";



    }
}
