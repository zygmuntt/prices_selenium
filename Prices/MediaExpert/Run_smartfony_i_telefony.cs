﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Prices.MediaExpert
{
public     class Run_smartfony_i_telefony
    {
        [Theory]
        [InlineData(MediaExpert.URLS.m_smartfony_i_telefony_akcesoria_do_telefonow)]
        [InlineData(MediaExpert.URLS.m_smartfony_i_telefony_cb_radio_telefony)]
        [InlineData(MediaExpert.URLS.m_smartfony_i_telefony_nawigacje_gps)]
        [InlineData(MediaExpert.URLS.m_smartfony_i_telefony_telefony_stacjonarne_i_faksy)]
        [InlineData(MediaExpert.URLS.s_smartfony_i_telefony_smartwatche_i_zegarki)]
        public void Run_multiple(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_multiple(url);
        }

        [Theory]
        [InlineData(MediaExpert.URLS.s_smartfony_i_telefony_gimbale)]
        [InlineData(MediaExpert.URLS.s_smartfony_i_telefony_okulary_vr)]
        [InlineData(MediaExpert.URLS.s_smartfony_i_telefony_smartfony)]
        [InlineData(MediaExpert.URLS.s_smartfony_i_telefony_telefony_komorkowe)]
        public void Run_single(string url)
        {
            var go = new Mediaexpert_check_pricese();
            go.check_prices_single(url);
        }
    }
}
